package auroras.OpenJDR.game;

import java.util.ArrayList;

import auroras.OpenJDR.client.defaultImpl.Caracts;
import auroras.OpenJDR.client.defaultImpl.Character;
import auroras.OpenJDR.client.defaultImpl.Competences;
import auroras.OpenJDR.client.defaultImpl.Inventory;
import auroras.OpenJDR.client.defaultImpl.map.DefaultMap;
import auroras.OpenJDR.client.defaultImpl.model.characters.Barbarian;
import auroras.OpenJDR.client.defaultImpl.model.characters.Elfe;
import auroras.OpenJDR.client.defaultImpl.model.characters.Gnome;
import auroras.OpenJDR.client.defaultImpl.model.characters.Human;
import auroras.OpenJDR.client.defaultImpl.model.characters.Troll;
import auroras.OpenJDR.client.defaultImpl.model.characters.Wizard;
import auroras.OpenJDR.client.interfaces.controller.IGameController;
import auroras.OpenJDR.client.interfaces.map.IFullMap;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.view.ICharacterView;
import auroras.OpenJDR.client.ui.MainView;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class OpenJDRGame {

	private static IGameController controller;

	public OpenJDRGame() {
	}
	
	public static void setController(IGameController controller) {
		OpenJDRGame.controller=controller;
	}
	
	public static IGameController getController() {
		return controller;
	}

	/** TODO: remove */
	public void initDefaultGroup() {
		controller.getMainModel().getCharacetrModelViewMapping().setCurrentCharacters(controller.getMainModel().getCharacetrModelViewMapping().getDefaultCharacters());		
	}
	
}
