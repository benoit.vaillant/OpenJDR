package auroras.OpenJDR.licences;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import auroras.OpenJDR.client.ui.IConstants;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.common.utils.FileUtils;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class ExternalLicences extends JPanel implements ITranslatableElement{
	
	private enum ExternalLicence {
		PREFUSE("Prefuse", "license-prefuse.txt"),
		APACHE("log4j", "apache2.txt"),
		CREATIVECOMMONS0("CC0", "CC0.txt"),
		BSD("BSD", "BSD.txt"),
		;
		
		private String licenceName;
		private String resourceFile;
		private String author;
		
		private ExternalLicence(String licenceName, String resourceFile) {
			this.licenceName=licenceName;
			this.resourceFile=resourceFile;
		}
		
		private ExternalLicence(String licenceName, String resourceFile, String author) {
			this.licenceName=licenceName;
			this.resourceFile=resourceFile;
			this.author=author;
		}
		
		
		public String getLicenceName() {
			return licenceName;
		}
		
		public File getResourceFile() {
			return new File("/resources/licences/",resourceFile);
		}
		
		public String getAuthor() {
			return author;
		}
		
	}

	private enum ExternalProject {
		PREFUSE("Prefuse", ExternalLicence.PREFUSE,null),
		APACHE("Log4j", ExternalLicence.APACHE,null),
		MIGLAYOUT("MigLayout", ExternalLicence.BSD, null),
		OPENCLIPART("openclipart", ExternalLicence.CREATIVECOMMONS0, "https://openclipart.org"),
		;
		
		private String projectName;
		private ExternalLicence licence;
		private String url;
		
		private ExternalProject(String projectName, ExternalLicence licence, String url) {
			this.projectName=projectName;
			this.licence=licence;
			this.url=url;
		}
		
		public String getProjectName() {
			return projectName;
		}
		
		public ExternalLicence getExternalLicence() {
			return licence;
		}
		
		public String getUrl() {
			return url;
		}
		
	}

	
	public ExternalLicences() {
		super(new BorderLayout());		
	}
	
	@Override
	public void buildUI(Language lang) {
		this.removeAll();
		this.add(new JLabel(Lang.get("External projects used", lang)), BorderLayout.NORTH);
		this.add(getInnerLicences(lang), BorderLayout.CENTER);		
	}

	private JPanel getInnerLicences(Language lang) {
		JPanel innerPane= new JPanel(new FlowLayout());
		
		ExternalProject[] extProjects= ExternalProject.values();
//		Arrays.sort(extProjects);
		
		for(ExternalProject project:extProjects) {
			JButton button = new JButton(project.getProjectName());
			button.setToolTipText(project.getExternalLicence().getLicenceName());
			
			button.addActionListener(displayLicenceDialog(project.getExternalLicence(),lang));
			innerPane.add(button);
		}
				
		return innerPane;
	}

	private static ActionListener displayLicenceDialog(ExternalLicence licence, Language lang) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				final JFrame licFrame = new JFrame(licence.getLicenceName());
				JPanel licPanel=new JPanel(new BorderLayout());
				
				JTextArea licField = new JTextArea();
				licField.setEditable(false);
				try {
					licField.setText(FileUtils.readResourceFileContents(licence.getResourceFile(), lang));
				} catch (IOException e1) {
					licField.setText(Lang.get("Failed to fetch licence content", lang));
					e1.printStackTrace();
				}
				if(licField.getText().length()==0)
					licField.setText(Lang.get("Author:", lang)+" "+licence.getAuthor());

				
				JScrollPane scrolly = new JScrollPane(licField);
				licPanel.add(scrolly, BorderLayout.CENTER);
				licFrame.add(licPanel);
				
				Box hBox=Box.createHorizontalBox();
				hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
				hBox.add(Box.createHorizontalGlue());
				JButton closeButton = new JButton(Lang.get("Close", lang));
				closeButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						licFrame.setVisible(false);
						licFrame.dispose();						
					}
				});
				
				hBox.add(closeButton);
				hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
				licPanel.add(hBox, BorderLayout.SOUTH);
				licFrame.pack();				
				licFrame.setVisible(true);
			}
		};
	}	
	
	
}
