package auroras.OpenJDR.editor.east;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ListCellRenderer;

import auroras.OpenJDR.client.ui.IConstants;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.client.ui.tiles.EmptyBorderedTile;
import auroras.OpenJDR.client.ui.tiles.EraserBodreredTile;
import auroras.OpenJDR.client.ui.tiles.TileEditorLocation;
import auroras.OpenJDR.client.ui.utils.JPanelUtils;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.common.utils.IconsAndImages;
import auroras.OpenJDR.common.utils.IconsAndImages.Rotation;
import auroras.OpenJDR.common.utils.IconsAndImages.TileBackground;
import net.miginfocom.swing.MigLayout;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class JTilePicker extends JPanel implements ITranslatableElement {

	//private JPanel commonTiles;
	
	public JTilePicker() {
		super(new BorderLayout());
	}
	
	@Override
	public void buildUI(Language lang) {
		this.removeAll();
		
		this.add(buildCommonTilesPane(lang), BorderLayout.NORTH);
		this.add(buildDungeonTilesPane(lang), BorderLayout.CENTER);				
	}

	private JPanel buildCommonTilesPane(Language lang) {
		JPanel commonTiles=new JPanel(new BorderLayout());
		Box hBox=Box.createHorizontalBox();
		hBox.add(new JLabel(Lang.get("Brushes", lang)));
		hBox.add(Box.createHorizontalGlue());
		commonTiles.add(hBox, BorderLayout.NORTH);
		
		JPanel utilityTilesPane = new JPanel(new GridLayout(1, 0));
		utilityTilesPane.add(new EmptyBorderedTile(Rotation.NONE));
		utilityTilesPane.add(new EraserBodreredTile(Rotation.NONE));
		
		
		JScrollPane scrolly = new JScrollPane(utilityTilesPane, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		commonTiles.add(scrolly, BorderLayout.CENTER);

		return JPanelUtils.createBorderedJPanel(commonTiles);
	}

	
	private JPanel buildDungeonTilesPane(Language lang) {
		JPanel pane = new JPanel(new BorderLayout());
		
		final JComboBox<TileEditorLocation> comboBox = new JComboBox<TileEditorLocation>();
				
		comboBox.setRenderer(new TileEditorLocationCellRenderer(lang));
		
		HashMap<TileEditorLocation, JPanel> tilePanels=new HashMap<>();
		
		for(TileEditorLocation loc: TileEditorLocation.values()) {
			comboBox.addItem(loc);
			JPanel tilePanel=new JPanel(new MigLayout("", "[]5[]5[]5[]",""));
			fillInnerPane(tilePanel, loc);
			tilePanels.put(loc, tilePanel);
		}

		pane.add(comboBox, BorderLayout.NORTH);
		final SubJTilePicker picker = new SubJTilePicker(tilePanels);
		pane.add(new JScrollPane(picker), BorderLayout.CENTER);

		comboBox.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				picker.setVisible((TileEditorLocation)comboBox.getSelectedItem());
			}
		});

		return JPanelUtils.createBorderedJPanel(pane);
	}

	private void fillInnerPane(JPanel innerPane, TileEditorLocation location) {
		int col=0;
		for(TileBackground bg:IconsAndImages.TileBackground.values()) {
			if(location.equals(bg.getLocation())) {
				try {
					innerPane.add((JComponent)bg.getTileReprClass().getDeclaredConstructor(Rotation.class).newInstance(Rotation.NONE), col==2?"wrap":"");
					col++;
					if(col>2)
						col=0;
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | NoSuchMethodException | SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
