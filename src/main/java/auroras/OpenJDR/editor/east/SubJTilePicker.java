package auroras.OpenJDR.editor.east;

import java.awt.BorderLayout;
import java.util.HashMap;

import javax.swing.JPanel;

import auroras.OpenJDR.client.ui.tiles.TileEditorLocation;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class SubJTilePicker extends JPanel {

	private HashMap<TileEditorLocation, JPanel> tilePanels;
	
	private JPanel oldPanel=null;
	
	public SubJTilePicker(HashMap<TileEditorLocation, JPanel> tilePanels) {
		super(new BorderLayout());
		this.tilePanels=tilePanels;
	}
	
	public void setVisible(TileEditorLocation location) {
		if(oldPanel!=null)
			this.remove(oldPanel);
		oldPanel=tilePanels.get(location);
		this.add(oldPanel, BorderLayout.CENTER);
		this.revalidate();
	}
	
}
