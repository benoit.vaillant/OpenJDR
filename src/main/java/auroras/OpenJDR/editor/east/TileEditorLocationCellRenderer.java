package auroras.OpenJDR.editor.east;

import java.awt.Color;
import java.awt.Component;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import auroras.OpenJDR.client.ui.IConstants;
import auroras.OpenJDR.client.ui.tiles.TileEditorLocation;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class TileEditorLocationCellRenderer implements ListCellRenderer<TileEditorLocation> {

	private static Language lang;
	
	public TileEditorLocationCellRenderer(Language lang) {
		TileEditorLocationCellRenderer.lang=lang;		
	}
	
	@Override
	public Component getListCellRendererComponent(JList<? extends TileEditorLocation> list,
			TileEditorLocation value, int index, boolean isSelected, boolean cellHasFocus) {
		JPanel pane = new JPanel();
		Box hBox=Box.createHorizontalBox();
		hBox.add(new JLabel(value.getUiIcon()));
		hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
		hBox.add(Box.createHorizontalGlue());
		hBox.add(new JLabel(Lang.get(value.getUiTranslatableName(), lang)));
		hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
		pane.add(hBox);
		return pane;
	}
	
}
