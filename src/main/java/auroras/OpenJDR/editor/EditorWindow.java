package auroras.OpenJDR.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;

import auroras.OpenJDR.client.factories.map.MapFactory;
import auroras.OpenJDR.client.ui.BottomBar;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.editor.east.JTilePicker;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class EditorWindow extends JFrame implements ITranslatableElement{

	private JPanel editorPane;
	
	/** map pane */
	private EditorGridMapPane mapPane;
	
	/** utilities panes */
	private EditorLevelTreePane leftPane;
	private JTilePicker rightPane;
	private EditorJMenuBar menubar;
	private EditorInfoBox bottomInfoBox;
	
	public EditorWindow(Language lang) {
		setTitle(Lang.get("Level editor", lang));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setPreferredSize(new Dimension(800, 600));
		
		editorPane = new JPanel(new BorderLayout());
		menubar=new EditorJMenuBar(this);
		editorPane.add(menubar, BorderLayout.NORTH);
		leftPane = new EditorLevelTreePane();
		editorPane.add(leftPane, BorderLayout.WEST);
		rightPane=new JTilePicker();
		editorPane.add(rightPane, BorderLayout.EAST);
		bottomInfoBox=new EditorInfoBox();
		editorPane.add(bottomInfoBox, BorderLayout.SOUTH);
		mapPane=new EditorGridMapPane(80,60);
		JScrollPane scrolly = new JScrollPane(mapPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		editorPane.add(scrolly, BorderLayout.CENTER);
		this.setContentPane(editorPane);
	}
	
	@Override
	public void buildUI(Language lang) {
		leftPane.buildUI(lang);
		rightPane.buildUI(lang);
		menubar.buildUI(lang);
		bottomInfoBox.buildUI(lang);
		mapPane.buildUI(lang);
	}

}
