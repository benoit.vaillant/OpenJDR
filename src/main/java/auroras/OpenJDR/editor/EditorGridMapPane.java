package auroras.OpenJDR.editor;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Point;
import java.util.HashMap;

import javax.swing.JPanel;

import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.client.ui.tiles.AbstractTile;
import auroras.OpenJDR.client.ui.tiles.EmptyBorderedTile;
import auroras.OpenJDR.client.ui.tiles.EmptyCorneredTile;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.common.utils.IconsAndImages.Rotation;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class EditorGridMapPane extends JPanel implements ITranslatableElement {

	private Language lang;
	
	private HashMap<Point, AbstractTile> tiles;
	private HashMap<Point, Rotation> rotations;
	
	public EditorGridMapPane(int width, int height) {
		super(new GridLayout(width,height));
		tiles=new HashMap<>();
		rotations=new HashMap<>();
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				tiles.put(new Point(i, j), new EmptyCorneredTile(Rotation.NONE));
				rotations.put(new Point(i,j), Rotation.NONE);
			}
		}
		
	}

	@Override
	public void buildUI(Language lang) {
		this.lang=lang;	
	}
	
	private Point convertToTileSize(int x, int y) {
		return new Point(x/AbstractTile.TILE_SIZE, y/AbstractTile.TILE_SIZE);
	}

	@Override
	public Component getComponentAt(int x, int y) {
		return getComponentAtAbstractPoint(convertToTileSize(x, y));
	}
	
	@Override
	public Component getComponentAt(Point p) {
		return getComponentAtAbstractPoint(convertToTileSize(p.x, p.y));
	}

	private Component getComponentAtAbstractPoint(Point p) {
		AbstractTile tile = tiles.get(p);
		if(tile==null) {
			tile=new EmptyBorderedTile(Rotation.NONE);
		}
		return tile;
	}
	
	private GridLayout getLayoutManager() {
		return (GridLayout)getLayout();
	}
	
	@Override
	public int getComponentCount() {
		GridLayout lm = getLayoutManager();
		return lm.getRows()*lm.getColumns();
	}
	
	@Override
	public Component getComponent(int n) {
		int row = n/getLayoutManager().getColumns();
		int col= n-row*getLayoutManager().getColumns();
		AbstractTile tile =tiles.get(new Point(row,col));
		if(tile==null)
			tile = new EmptyBorderedTile(Rotation.NONE);
		return tile;
	}
}
