package auroras.OpenJDR.editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class EditorJMenuBar extends JMenuBar implements ITranslatableElement{

	private EditorWindow editor;
	
	public EditorJMenuBar(EditorWindow editor) {
		this.editor=editor;
	}
	
	@Override
	public void buildUI(Language lang) {
		removeAll();
		
		JMenu fileMenu = new JMenu(Lang.get("File", lang));
		
		JMenuItem openMenu = new JMenuItem(Lang.get("Open", lang));
		JMenuItem saveMenu = new JMenuItem(Lang.get("Save", lang));
		JMenuItem closeMenu = new JMenuItem(Lang.get("Close", lang));
		
		closeMenu.addActionListener(new ActionListener() {
			
			/** TODO: check for unsaved work */
			@Override
			public void actionPerformed(ActionEvent e) {
				editor.dispose();
			}
		});
				
		fileMenu.add(openMenu);
		fileMenu.add(saveMenu);
		fileMenu.add(closeMenu);
		this.add(fileMenu);
		
	}
	
}
