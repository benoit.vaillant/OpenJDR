package auroras.OpenJDR.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import auroras.OpenJDR.client.ui.MainView;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class FileUtils {

	private static MainView mainView;

	public static void init(MainView mainView) {
		if(FileUtils.mainView!=null) {
			System.err.println("FileUtils class already initialized!");
			return;
		}
		FileUtils.mainView=mainView;
	}

	
	public static String readResourceFileContents(File file, Language lang) throws IOException {
//		if(!file.exists()) {
//			throw new IOException(Lang.get("Could not find file: ", lang)+file.getCanonicalPath());
//		}
		java.net.URL fileURL = mainView.getClass().getResource(file.getCanonicalPath());
		BufferedReader reader = new BufferedReader(new FileReader(fileURL.getFile()));
		StringBuffer sb = new StringBuffer();
		String line;
		boolean first=true;
		while((line=reader.readLine())!=null) {
			if(!first) {
				sb.append("\n");
			}
			first=false;
			sb.append(line);
		}
		return sb.toString();
	}

}
