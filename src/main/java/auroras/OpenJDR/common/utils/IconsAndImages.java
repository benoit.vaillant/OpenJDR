package auroras.OpenJDR.common.utils;

import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.Border;

import auroras.OpenJDR.client.ui.tiles.*;
import auroras.OpenJDR.client.ui.tiles.roads.CrossRoad;
import auroras.OpenJDR.client.ui.tiles.roads.SimpleRoad;
import auroras.OpenJDR.client.ui.tiles.roads.TRoad;
import auroras.OpenJDR.client.ui.tiles.roads.TurnRoad;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;
import auroras.OpenJDR.client.ui.MainView;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class IconsAndImages {

	private static MainView mainView;

	public static enum Rotation {
		NONE(0),
		LEFT(-90),
		RIGTH(90),
		FLIP(180);
		
		private int rotationAngle;
		
		Rotation(int rotationAngle) {
			this.rotationAngle=rotationAngle;
		}
		
		public int getRotationAngle() {
			return rotationAngle;
		}
		
		public static double convertDegreesToRadians(int degrees) {
			return degrees*Math.PI/180.0;
		}
		
	}

	public static ImageIcon playerImageIcon;
	public static ImageIcon groupImageIcon;
	public static ImageIcon undoIcon;
	public static ImageIcon redoIcon;
	public static ImageIcon undoIconGray;
	public static ImageIcon redoIconGray;

	public static void init(MainView mainView) {
		if(IconsAndImages.mainView!=null) {
			System.err.println("IconsAndImages class already initialized!");
			return;
		}
		IconsAndImages.mainView=mainView;
		playerImageIcon = IconsAndImages.createImageIcon("/resources/png/character/icons/icon-person-32.png");
		groupImageIcon = IconsAndImages.createImageIcon("/resources/png/character/icons/icon-group-32.png");
		undoIcon = IconsAndImages.createImageIcon("/resources/png/ui/icons/undo.png");
		redoIcon = IconsAndImages.createImageIcon("/resources/png/ui/icons/redo.png");
		undoIconGray = IconsAndImages.createImageIcon("/resources/png/ui/icons/undoGray.png");
		redoIconGray = IconsAndImages.createImageIcon("/resources/png/ui/icons/redoGray.png");
	}

	
	/** do not use abusively since it loads the images at each call */
	public static ImageIcon createImageIcon(String path) {
		java.net.URL imgURL = mainView.getClass().getResource(path);
		if (imgURL != null) {
			GameLogger.getLogger(LogModule.VIEW).debug("image loaded: %s", imgURL.getPath());
			return new ImageIcon(imgURL);
		} else {
			GameLogger.getLogger(LogModule.VIEW).error("Couldn't find file: %s", path);
			return null;
		}
	}
	
	public static enum TileBackground {
		EMPTY_BORDERED_TILE("/resources/png/tiles/EmptyBorderedTile.png", EmptyBorderedTile.class, null),
		EMPTY_CORNERED_TILE("/resources/png/tiles/EmptyCorneredTile.png", EmptyCorneredTile.class, null),
		DEFAULT_TILE("/resources/png/tiles/DefaultTile.png", DefaultTile.class, TileEditorLocation.MAIN_MAP),
		SIMPLE_ROAD("/resources/png/tiles/roads/SimpleRoad.png", SimpleRoad.class, TileEditorLocation.MAIN_MAP),
		TURN_ROAD("/resources/png/tiles/roads/TurnRoad.png", TurnRoad.class, TileEditorLocation.MAIN_MAP),
		T_ROAD("/resources/png/tiles/roads/TRoad.png", TRoad.class, TileEditorLocation.MAIN_MAP),
		CROSS_ROAD("/resources/png/tiles/roads/CrossRoad.png", CrossRoad.class, TileEditorLocation.MAIN_MAP),
		;
		
		private ImageIcon imageIcon;
		private Class reprClassName;
		private TileEditorLocation location;
		
		TileBackground(String file, Class reprClassName, TileEditorLocation location) {
			java.net.URL imgURL = mainView.getClass().getResource(file);
			imageIcon=new ImageIcon(imgURL);
			this.reprClassName=reprClassName;
			this.location=location;
		}
		
		public ImageIcon getImageIcon() {
			return imageIcon;
		}
		
		public Class getTileReprClass() {
			return reprClassName;
		}

		public TileEditorLocation getLocation() {
			return location;
		}
		
	}
	
	public static void removeButtonDecorators(JButton...buttons) {
		for(JButton b:buttons) {
			//Border emptyBorder = BorderFactory.createEmptyBorder();
			b.setBorder(null);
			b.setBorderPainted(false);
			b.setRolloverEnabled(false);
			b.setContentAreaFilled(false);
		}
	}
	
}
