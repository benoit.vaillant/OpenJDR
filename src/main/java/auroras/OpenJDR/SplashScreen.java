package auroras.OpenJDR;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;

import auroras.OpenJDR.client.OpenJDR;
import auroras.OpenJDR.client.ui.IConstants;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class SplashScreen extends JWindow {

    private static JProgressBar pbar;
    private static JLabel infoLabel;
    
    private static Language lang;
	
	public SplashScreen(Language lang, URL url) {
		super();
		
		SplashScreen.lang=lang;
		
		setLocationRelativeTo(null);
        setAlwaysOnTop(true);
        
		JPanel pane = new JPanel(new BorderLayout());
		pane.add(new JLabel(new ImageIcon(url)), BorderLayout.CENTER);
		
		Box vBox = Box.createVerticalBox();
		vBox.add(Box.createVerticalStrut(IConstants.V_GAP));
		
		
		Box hBox = Box.createHorizontalBox();
		hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
		infoLabel = new JLabel(Lang.get("Loading", lang));
		hBox.add(infoLabel);
		hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
		hBox.add(new JLabel(Lang.get("Starting", lang)));
		hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
		
		vBox.add(hBox);
		
		hBox = Box.createHorizontalBox();
		pbar = new JProgressBar();
        pbar.setMinimum(1);
        pbar.setMaximum(OpenJDR.getConstructorsInOrder().length+1);
        pbar.setStringPainted(true);
        pbar.setForeground(Color.LIGHT_GRAY);
        
        hBox.add(pbar);
        
		hBox.add(Box.createHorizontalGlue());
		hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));

		vBox.add(Box.createVerticalStrut(IConstants.V_GAP));
		
		vBox.add(hBox);
		
		pane.add(vBox, BorderLayout.SOUTH);
		this.setContentPane(pane);		
		
		Dimension dim =  pane.getPreferredSize();

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		setLocation(screenSize.width/2 - (dim.width/2), screenSize.height/2 - (dim.height/2));
		
		this.pack();
		
	}
	
	public void update(String s) {
		infoLabel.setText(Lang.get(s, lang));
		this.update(getGraphics());
		//this.repaint();
		pbar.setValue(pbar.getValue()+1);
	}
	
}
