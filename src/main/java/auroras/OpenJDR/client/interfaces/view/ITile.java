package auroras.OpenJDR.client.interfaces.view;

import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.model.IItem;
import auroras.OpenJDR.common.utils.IconsAndImages.TileBackground;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public interface ITile {

	boolean hasItems();
	
	int getCharacterNumber();
	
	boolean hasGroup();
	
	void addCharacter(ICharacter character);
	
	boolean removeCharacter(ICharacter character);
	
	void addItem(IItem item);
	
	boolean removeItem(IItem item);
	
	TileBackground getTileBackground();
	
	boolean canRotate();
	
}
