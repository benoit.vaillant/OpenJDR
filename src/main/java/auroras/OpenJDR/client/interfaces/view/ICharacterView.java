package auroras.OpenJDR.client.interfaces.view;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;

import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.ui.ITranslatableElement;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public interface ICharacterView extends ITranslatableElement {

	public JPanel getView();

	public int getHeight();

	public Icon getPersoImage();

	public Component getCaracterRepr();

	public Component getCaracteristicsRepr();

	public Component getCompetencesRepr();
	
	public void setEditableName(boolean edit);
	
}
