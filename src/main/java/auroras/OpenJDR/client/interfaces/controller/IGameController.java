package auroras.OpenJDR.client.interfaces.controller;

import java.awt.Component;
import java.util.ArrayList;

import auroras.OpenJDR.client.defaultImpl.model.MainModel;
import auroras.OpenJDR.client.interfaces.map.IFullMap;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.model.IQuest;
import auroras.OpenJDR.client.interfaces.view.IPath;
import auroras.OpenJDR.client.ui.MainView;
import auroras.OpenJDR.game.OpenJDRGame;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public interface IGameController {

public void loadGameImpl();
	
	public Class getImpl(String className);
	
	public void setMap(IFullMap map);
	
	public IFullMap getMap();

	public void updateSelectedCharacter(ICharacter valueAt);

	public void quit();
	
	public MainView getMainView();

	public MainModel getMainModel();

	public ArrayList<IQuest> getQuests();
	
	public boolean hasQuests();
	
	public void setCheatMode(boolean on);
	
	public void updateUndoRedo();
	
	public void undo();
	
	public void redo(boolean reroll);

}
