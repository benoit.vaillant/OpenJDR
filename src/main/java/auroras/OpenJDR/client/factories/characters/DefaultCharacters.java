package auroras.OpenJDR.client.factories.characters;

import java.lang.reflect.InvocationTargetException;

import auroras.OpenJDR.client.abstractImpl.model.AbstractCharacterImpl;
import auroras.OpenJDR.client.exceptions.init.InvalidCharacterCreationException;
import auroras.OpenJDR.client.interfaces.model.ICaracteristics;
import auroras.OpenJDR.client.interfaces.model.ICompetences;
import auroras.OpenJDR.client.interfaces.model.IInventory;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.game.OpenJDRGame;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;
import auroras.OpenJDR.client.defaultImpl.Caracts;
import auroras.OpenJDR.client.defaultImpl.Competences;
import auroras.OpenJDR.client.defaultImpl.Inventory;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class DefaultCharacters  {

	
	private static AbstractCharacterImpl defaultcharacter;

	public static void init() {
		//TODO
	}
	
	public static AbstractCharacterImpl emptyCharacter(Language lang) throws InvalidCharacterCreationException {
		if(defaultcharacter==null) {
			defaultcharacter=emptyCharacter("undefined", lang);
		}
		return defaultcharacter;
	}

	public static AbstractCharacterImpl emptyCharacter(String name, Language lang) throws InvalidCharacterCreationException {
		try {
			return (AbstractCharacterImpl)OpenJDRGame.getController().getImpl("character").getDeclaredConstructor(String.class, ICaracteristics.class, ICompetences.class, IInventory.class).newInstance("undefined", new Caracts(), new Competences(), new Inventory());
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {			
			GameLogger.getLogger(LogModule.VIEW).error("Could not instantiate default character: %s", e.getMessage());
		}
		throw new InvalidCharacterCreationException(Lang.get("Cannot create empty character", lang));
	}

}
