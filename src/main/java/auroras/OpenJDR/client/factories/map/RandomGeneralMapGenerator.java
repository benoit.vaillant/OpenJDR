package auroras.OpenJDR.client.factories.map;

import java.util.ArrayList;
import java.util.Collections;

import auroras.OpenJDR.client.defaultImpl.map.DefaultGeneralMap;
import auroras.OpenJDR.client.interfaces.view.ITile;
import auroras.OpenJDR.client.ui.tiles.DefaultTile;
import auroras.OpenJDR.client.ui.tiles.roads.CrossRoad;
import auroras.OpenJDR.client.ui.tiles.roads.SimpleRoad;
import auroras.OpenJDR.client.ui.tiles.roads.TRoad;
import auroras.OpenJDR.common.utils.Dices;
import auroras.OpenJDR.common.utils.IconsAndImages.Rotation;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class RandomGeneralMapGenerator {

	/** probabilities should sum up to 100% */
	public static enum Ground {
		defGround(75),
		road(25),
		;
		
		private int proba;
		
		private static Ground[] groundHelper;
		
		Ground(int proba) {
			this.proba=proba;
		}
		
		public int getProba() {
			return proba;
		}
		
		/** maybe not really needed */
		public static void regenerateGroundHelper() {
			ArrayList<Ground> grounds =new ArrayList<>();
			for(Ground g:Ground.values()) {
				for(int i = 0 ; i< g.getProba(); i++) {
					grounds.add(g);
				}
			}
			if(grounds.size()!=100) {
				throw new RuntimeException("Invalid random ground tile numbers");
			}
			Collections.shuffle(grounds);
			groundHelper=grounds.toArray(new Ground[100]);
		}
	}
	
	public static DefaultGeneralMap generateRandomGeneralMap(int width, int height) {
		
		Ground[][] grounds = new Ground[width][height];
		for (int i = 0; i < grounds.length; i++) {
			for (int j = 0; j < grounds[0].length; j++) {
				grounds[i][j]=Ground.groundHelper[Dices.rollDice(100)-1];
			}
		}
		
		ITile[][] tiles=new ITile[width][height];
		Rotation[][] rotations = new Rotation[width][height];
		for (int i = 0; i < grounds.length; i++) {
			for (int j = 0; j < grounds[0].length; j++) {
				updateTileAndRotationAt(grounds, i,j,tiles);
			}
		}
				
		return new DefaultGeneralMap(tiles, rotations);
	}

	/** TODO: adapt to the more general case where corners can be roads too */
	private static void updateTileAndRotationAt(Ground[][] grounds, int i, int j, ITile[][] tiles) {
		switch (grounds[i][j]) {
		case road:
			Ground[][] virtGround = virtualAdjascingGround(grounds, i, j);
			tiles[i][j]=getRoadRep(virtGround);			
			break;
		case defGround:
		default:
			tiles[i][j]=new DefaultTile(Rotation.NONE);
			break;
		}
		
	}
	
	private static Ground[][] virtualAdjascingGround(Ground[][] grounds, int i, int j) {
		Ground[][] ret = new Ground[3][3];
		for (int k = 0; k < ret.length; k++) {
			for (int l = 0; l < ret[0].length; l++) {
				ret[k][l]=Ground.defGround;
			}
		}
		for(int k=-1; k<2;k++) {
			for(int l=-1; l<2;l++) {
				if(i+k>=0 && i+k<grounds.length &&
					j+l>=0 && j+l<grounds[0].length) {
					ret[k+1][l+1]=grounds[i+k][j+l];
				}
			}			
		}
		return ret;
	}
	
	/** TODO */
	private static ITile getRoadRep(Ground[][] virtGround) {
		int nbAdjascingRoads=0;
		if(virtGround[0][1].equals(Ground.road))
			nbAdjascingRoads++;
		if(virtGround[1][0].equals(Ground.road))
			nbAdjascingRoads++;
		if(virtGround[1][2].equals(Ground.road))
			nbAdjascingRoads++;
		if(virtGround[2][1].equals(Ground.road))
			nbAdjascingRoads++;

		switch (nbAdjascingRoads) {
		case 4:
			return new CrossRoad(Rotation.NONE);
		case 3:
			return new TRoad(Rotation.NONE);
		case 2:
			return new SimpleRoad(Rotation.NONE);
		case 1:
			
			break;

		default://should not happen
			return new DefaultTile(Rotation.NONE);
		}
		//should not happen
		return new DefaultTile(Rotation.NONE);
	}

	
}
