package auroras.OpenJDR.client.factories.map;

import java.awt.Component;
import java.awt.GridLayout;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JComponent;
import javax.swing.JPanel;

import auroras.OpenJDR.client.interfaces.map.IDungeonMap;
import auroras.OpenJDR.client.interfaces.map.IMap;
import auroras.OpenJDR.client.interfaces.model.IDungeon;
import auroras.OpenJDR.client.interfaces.view.ITile;
import auroras.OpenJDR.common.utils.IconsAndImages.Rotation;
import auroras.OpenJDR.editor.EditorGridMapPane;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class MapFactory {

	public static JPanel buildGeneralMapRepresentation(IMap map) {
		JPanel pane = new JPanel(new GridLayout(map.getWidth(), map.getHeight()));
		fillWithTiles(pane, map);
		return pane;
	}

	public static JPanel buildDungeonMapRepresentation(IDungeon dungeon, int level) {
		IDungeonMap map = dungeon.getLevels().get(level);
		JPanel pane = new JPanel(new GridLayout(map.getWidth(), map.getHeight()));
		fillWithTiles(pane, map);		
		return pane;
	}

	private static void fillWithTiles(JPanel pane, IMap map) {
		for (int i = 0; i < map.getWidth(); i++) {
			for (int j = 0; j < map.getHeight(); j++) {
				ITile tile = map.getTileAt(i, j);
				try {
					JComponent repr = (JComponent)tile.getTileBackground().getTileReprClass().
							getDeclaredConstructor(Rotation.class).
							newInstance(map.getTileRotationAt(i, j));
					pane.add(repr);
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | NoSuchMethodException | SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}		
	}

}
