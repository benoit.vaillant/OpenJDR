package auroras.OpenJDR.client.factories.map;

import java.util.ArrayList;

import auroras.OpenJDR.client.defaultImpl.map.DefaultMap;
import auroras.OpenJDR.client.interfaces.map.IFullMap;
import auroras.OpenJDR.common.utils.Dices;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class RandomFullMapGenerator {

	/** TODO: make more random choices */
	public static IFullMap generateFullMap() {		
		
		/** suboptimal */
		IFullMap map = new DefaultMap();
		
		map.setGeneralMap(
				RandomGeneralMapGenerator.generateRandomGeneralMap(
						map.getGeneralMap().getWidth(),
						map.getGeneralMap().getHeight()));
		
		/** TODO */
		map.setDungeons(new ArrayList<>());
		
		return map;
	}
	
	
}
