package auroras.OpenJDR.client.ui;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingWorker;
import javax.swing.border.BevelBorder;

import auroras.OpenJDR.client.factories.map.MapFactory;
import auroras.OpenJDR.client.interfaces.controller.IGameController;
import auroras.OpenJDR.client.interfaces.map.IFullMap;
import auroras.OpenJDR.client.ui.events.EventsAndChatPane;
import auroras.OpenJDR.client.ui.icons.DefaultIcon;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class CenterPane extends JPanel implements ITranslatableElement {

	
	private EventsAndChatPane eventsAndChatPane;
	private JPanel mapPane;

	public CenterPane() {
		super(new BorderLayout());
		mapPane=new JPanel(new BorderLayout());
		mapPane.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		this.add(mapPane, BorderLayout.CENTER);
		eventsAndChatPane=new EventsAndChatPane();
		this.add(eventsAndChatPane, BorderLayout.SOUTH);
	}

	public void buildUI(Language lang) {
		this.removeAll();
		this.add(mapPane, BorderLayout.CENTER);
		eventsAndChatPane.buildUI(lang);
		this.add(eventsAndChatPane, BorderLayout.SOUTH);
	}

	public void setMap(IFullMap map) {
		mapPane.removeAll();
		if(map.hasGeneralMap()) {
			mapPane.add(MapFactory.buildGeneralMapRepresentation(map.getGeneralMap()), BorderLayout.CENTER);
		} else {
			//this is a single dungeon
			mapPane.add(MapFactory.buildDungeonMapRepresentation(map.getDungeons().get(0),0));
		}
		JScrollPane scrolly = new JScrollPane(MapFactory.buildGeneralMapRepresentation(map.getGeneralMap()), ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrolly.getHorizontalScrollBar().setUnitIncrement(DefaultIcon.ICON_SIZE/2);
		scrolly.getVerticalScrollBar().setUnitIncrement(DefaultIcon.ICON_SIZE/2);
		mapPane.add(scrolly, BorderLayout.CENTER);
	}

}
