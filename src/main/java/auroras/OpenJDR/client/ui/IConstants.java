package auroras.OpenJDR.client.ui;

import javax.swing.JLabel;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public interface IConstants {

	public static final int H_GAP=10;
	public static final int V_GAP=10;
	
	public static final long SLEEP_TIME = 500;
	
	public static final int NUMBER_WIDTH= Double.valueOf(new JLabel("100").getPreferredSize().getWidth()).intValue();
	
}
