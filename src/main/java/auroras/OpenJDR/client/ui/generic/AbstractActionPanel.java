package auroras.OpenJDR.client.ui.generic;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.JXTable;

import auroras.OpenJDR.client.defaultImpl.model.CharacterGroup;
import auroras.OpenJDR.client.interfaces.controller.IGameController;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.client.ui.icons.DefaultIcon;
import auroras.OpenJDR.client.ui.icons.PlayIcon;
import auroras.OpenJDR.client.ui.utils.IconTableCellRenderer;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.common.utils.IconsAndImages.Rotation;
import auroras.OpenJDR.game.OpenJDRGame;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public abstract class AbstractActionPanel extends JPanel implements ITranslatableElement {
	
	private JXTable initiativeTable;
	private DefaultTableModel initiativeModel;
	
	public AbstractActionPanel() {
		super(new BorderLayout());
	}
	
	@Override
	public void buildUI(Language lang) {
		removeAll();
		this.add(new JLabel(Lang.get("Initiative", lang)), BorderLayout.NORTH);
		
		/** TODO: sort by initiative */
		CharacterGroup characters = OpenJDRGame.getController().getMainModel().getCharacetrModelViewMapping().getCurrentCharacters();
		Object[][] initiativeData = new Object[characters.size()][2];
		for (int i = 0; i < characters.size(); i++) {
			initiativeData[i][0]=characters.get(i).getName();
			initiativeData[i][1]=new PlayIcon(Rotation.RIGTH);
		}
		initiativeModel = new DefaultTableModel(initiativeData, new String[] {"", ""});
		initiativeTable =new JXTable(initiativeModel) {
			@Override
			public Class<?> getColumnClass(int column) {
				if(column==0)
					return String.class;
				return DefaultIcon.class;
			}
		};
		initiativeTable.setDefaultRenderer(DefaultIcon.class, new IconTableCellRenderer());
		//int tableWidth= initiativeTable.getWidth();
		//initiativeTable.getColumnExt(0).setPreferredWidth(tableWidth);
		initiativeTable.getColumnExt(1).setPreferredWidth((int)DefaultIcon.DEFAULT_ICON_DIMENSION.getWidth());
		
		this.add(initiativeTable, BorderLayout.CENTER);
	}
	
	
}
