package auroras.OpenJDR.client.ui.generic;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.JXTable;

import auroras.OpenJDR.client.interfaces.controller.IGameController;
import auroras.OpenJDR.client.interfaces.model.IQuest;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.game.OpenJDRGame;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public abstract class AbstractMissionPanel extends JPanel implements ITranslatableElement {

	public AbstractMissionPanel() {
		super(new BorderLayout());
	}
	
	@Override
	public void buildUI(Language lang) {
		this.removeAll();
		this.add(new JLabel(Lang.get("Quests", lang)), BorderLayout.NORTH);
		
		JPanel missionPanel = new JPanel(new BorderLayout());
		ArrayList<IQuest> missions = OpenJDRGame.getController().getQuests();

		Object[][] misssionModel = new Object[missions.size()][];
		for (int i = 0; i < misssionModel.length; i++) {
			misssionModel[i][0]=Lang.get(missions.get(i).getMissionName(), lang);
		}

		DefaultTableModel missonModel = new DefaultTableModel(misssionModel, new String[] {""});
		JXTable missionTable = new JXTable(missonModel);
		
		JTextArea missionDescription = new JTextArea();
		
		
		JPanel innerPanel = new JPanel();
		innerPanel.setLayout(new BoxLayout(innerPanel, BoxLayout.Y_AXIS));
		
		innerPanel.add(missionTable);
		innerPanel.add(missionDescription);
		
		missionPanel.add(innerPanel, BorderLayout.CENTER);		
		
		//this.setVisible(controller.hasMisssions());
	}
	
	public abstract void checkEnd();

}
