package auroras.OpenJDR.client.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutionException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingWorker;

import auroras.OpenJDR.client.OpenJDR;
import auroras.OpenJDR.client.defaultImpl.map.DefaultGeneralMap;
import auroras.OpenJDR.client.defaultImpl.map.DefaultMap;
import auroras.OpenJDR.client.factories.map.RandomFullMapGenerator;
import auroras.OpenJDR.client.interfaces.controller.IGameController;
import auroras.OpenJDR.client.interfaces.map.IFullMap;
import auroras.OpenJDR.client.ui.group.creation.GroupCreation;
import auroras.OpenJDR.client.ui.utils.JPanelUtils;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.common.utils.IconsAndImages;
import auroras.OpenJDR.editor.EditorWindow;
import auroras.OpenJDR.game.OpenJDRGame;
import auroras.OpenJDR.licences.ExternalLicences;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class TopMenu extends JMenuBar implements ITranslatableElement {


	private ExternalLicences externalLicences;
	
	private JRadioButtonMenuItem cheatOn;
	private JRadioButtonMenuItem cheatOff;
	private CheatBox cheatBox;
	
	public TopMenu() {
		externalLicences=new ExternalLicences();
		cheatBox = new CheatBox();
	}

	public void buildUI(Language lang) {
		this.removeAll();
		this.add(createMainMenu(lang));
		
		this.add(createGroupMenu(lang));
		
		cheatOff= new JRadioButtonMenuItem(Lang.get("Off", lang));
		cheatOn= new JRadioButtonMenuItem(Lang.get("On", lang));
		
		externalLicences.buildUI(lang);
		this.add(createHelpMenu(lang));
		
		this.add(Box.createHorizontalGlue());
		cheatBox.buildUI(lang);
		this.add(cheatBox);
		cheatBox.setVisible(cheatOn.isSelected());
		OpenJDRGame.getController().updateUndoRedo();
	}

	private JMenu createMainMenu(Language lang) {
		JMenu menu =new JMenu(Lang.get("OpenJDR",lang));

		menu.add(gameMenu(lang));
		menu.add(cheatMenu(lang));

		JMenuItem quit = new JMenuItem(Lang.get("Quit", lang));
		quit.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				OpenJDRGame.getController().quit();
			}
		});

		menu.add(quit);

		return menu;
	}

	private JMenu createHelpMenu(Language lang) {
		JMenu menu = new JMenu(Lang.get("Help", lang));
		
		JMenuItem mainHelp = createMainHelpMenu(lang);
		mainHelp.setEnabled(false);
		menu.add(mainHelp);
		
		menu.add(createAboutMenu(lang));
		menu.add(createLicencesMenu(lang));
		
		return menu;
	}



	/** TODO: trigger some actions if in an already opened game */
	private JMenuItem gameMenu(final Language lang) {
		
		JMenu gameMenu = new JMenu(Lang.get("Game", lang));
		JMenu singlePlayerMenu = new JMenu(Lang.get("Single Player", lang));
		JMenu multiPlayerMenu = new JMenu(Lang.get("Multiplayer", lang));
		JMenuItem mapEditorMenuItem = new JMenuItem(Lang.get("Map Editor", lang));
		multiPlayerMenu.setEnabled(false);
		gameMenu.add(singlePlayerMenu);
		gameMenu.add(multiPlayerMenu);
		gameMenu.add(mapEditorMenuItem);
		
		JMenuItem newRandomGame=new JMenuItem(Lang.get("Random game", lang));
		newRandomGame.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingWorker<IFullMap, Void> worker = new SwingWorker<IFullMap, Void>() {
					@Override
					protected IFullMap doInBackground() throws Exception {						
						return RandomFullMapGenerator.generateFullMap();
					}
					
					@Override
					protected void done() {
						try {
							OpenJDRGame.getController().setMap(get());
							OpenJDRGame.getController().getMainView().buildUI(lang);							
						} catch (InterruptedException | ExecutionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
				};
				worker.execute();
				
				OpenJDRGame game = new OpenJDRGame();				
			}
		});
		singlePlayerMenu.add(newRandomGame);
		
		JMenuItem newEmptyGame=new JMenuItem(Lang.get("Empty game", lang));
		newEmptyGame.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				OpenJDRGame game = new OpenJDRGame();
				IFullMap map = new DefaultMap();
				OpenJDRGame.getController().setMap(map);
				OpenJDRGame.getController().getMainView().buildUI(lang);
			}
		});
		singlePlayerMenu.add(newEmptyGame);
		singlePlayerMenu.addSeparator();
		
		JMenuItem saveGame = new JMenuItem(Lang.get("Save", lang));
		saveGame.setEnabled(false);
		singlePlayerMenu.add(saveGame);
		
		JMenuItem loadGame = new JMenuItem(Lang.get("Load", lang));
		loadGame.setEnabled(false);
		singlePlayerMenu.add(loadGame);
		
		
		mapEditorMenuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Thread editorThread = new Thread(new Runnable() {
					@Override
					public void run() {
						EditorWindow window = new EditorWindow(lang);						
						window.setMinimumSize(window.getPreferredSize());
						window.buildUI(lang);
						window.setVisible(true);
					}
				});
				editorThread.run();
			}
		});
		
		return gameMenu;
	}

	private JMenuItem cheatMenu(final Language lang) {
		JMenu cheatItem = new JMenu(Lang.get("Cheat mode", lang));
		cheatOff= new JRadioButtonMenuItem(Lang.get("Off", lang));
		cheatOn= new JRadioButtonMenuItem(Lang.get("On", lang));
		ButtonGroup bGroup = new ButtonGroup();
		bGroup.add(cheatOn);
		bGroup.add(cheatOff);		
		
		cheatOff.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				OpenJDRGame.getController().setCheatMode(false);				
			}
		});

		cheatOn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				OpenJDRGame.getController().setCheatMode(true);				
			}
		});

		cheatItem.add(cheatOn);
		cheatItem.add(cheatOff);
		cheatOff.setSelected(true);
		
		return cheatItem;
	}

	private JMenu createGroupMenu(Language lang) {
		JMenu groupMenu = new JMenu(Lang.get("Group", lang));
		JMenuItem createGroup = new JMenuItem(Lang.get("Create group", lang));
		JMenuItem loadGroup = new JMenuItem(Lang.get("Load group", lang));
		JMenuItem saveGroup = new JMenuItem(Lang.get("save group", lang));
		
		createGroup.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Thread groupThread = new Thread(new Runnable() {
					@Override
					public void run() {
						GroupCreation groupCreation = new GroupCreation(lang);
						groupCreation.setMinimumSize(groupCreation.getPreferredSize());
						groupCreation.buildUI(lang);
						groupCreation.pack();
						groupCreation.setVisible(true);
					}
				}
			);
			groupThread.run();
			}
		});
		
		groupMenu.add(createGroup);
		groupMenu.addSeparator();
		groupMenu.add(loadGroup);
		groupMenu.add(saveGroup);
		return groupMenu;
	}

	
	/** TODO */
	private JMenuItem createMainHelpMenu(Language lang) {
		JMenuItem item = new JMenuItem(Lang.get("General help", lang));
		return item;
	}
	
	private JMenuItem createAboutMenu(Language lang) {
		JMenuItem item = new JMenuItem(Lang.get("About", lang));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame frame = new JFrame(Lang.get("About", lang));
				
				JPanel pane = new JPanel();
				pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
				pane.add(new JLabel("OpenJDR"));
				pane.add(new JLabel(Lang.get("Author:", lang)+" Benoît Vaillant"));
				pane.add(new JLabel("benoit.vaillant@auroras.fr"));
				pane.add(new JLabel(Lang.get("version:", lang)+" "+OpenJDR.getGameVersion()));

				frame.add(JPanelUtils.createBorderedJPanel(pane));
				frame.pack();
				frame.setVisible(true);
			}
		});
		return item;
	}
	
	private JMenuItem createLicencesMenu(Language lang) {
		JMenuItem item = new JMenuItem(Lang.get("Licences", lang));
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame frame = new JFrame(Lang.get("External tools and licences", lang));
				
				frame.add(JPanelUtils.createBorderedJPanel(externalLicences));
				
				frame.pack();
				frame.setVisible(true);
			}
		});
		return item;
	}

	public CheatBox getCheatBox() {
		return cheatBox;
	}

	
}