package auroras.OpenJDR.client.ui.utils;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import auroras.OpenJDR.client.abstractImpl.view.AbstractCharacterView;
import auroras.OpenJDR.client.exceptions.init.InvalidCharacterCreationException;
import auroras.OpenJDR.client.factories.characters.DefaultCharacters;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.view.ICharacterView;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.game.OpenJDRGame;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class JCharacterCellRenderer implements TableCellRenderer {

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {

		JPanel pane=new JPanel(new BorderLayout());
		try {
			pane = AbstractCharacterView.emptyPane(DefaultCharacters.emptyCharacter(Language.EN));
		} catch (InvalidCharacterCreationException e) {
			GameLogger.getLogger(LogModule.VIEW).error("Could not create character: %s", e.getMessage());
		} finally {
			pane = new JPanel(new BorderLayout());
			pane.add(new JLabel("internal error"), BorderLayout.CENTER);
		}

		try {
			ICharacter c = (ICharacter)value;
			ICharacterView cView = OpenJDRGame.getController().getMainModel().getCharacetrModelViewMapping().getView(c);
			pane = cView.getView();

			if(isSelected) {
				pane.setBackground(Color.lightGray);
			} else {
				pane.setBackground(Color.white);
			}
		} catch (IllegalArgumentException | SecurityException e) {
			GameLogger.getLogger(LogModule.VIEW).error("Could not view character: %s", e.getMessage());
		}

		return pane;
	}
}
