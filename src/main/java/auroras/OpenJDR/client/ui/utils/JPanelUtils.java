package auroras.OpenJDR.client.ui.utils;

import java.awt.BorderLayout;

import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JPanel;

import auroras.OpenJDR.client.ui.IConstants;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class JPanelUtils {

	public static JPanel createBorderedJPanel(JComponent component) {
		JPanel outerPane=new JPanel(new BorderLayout());
		outerPane.add(component, BorderLayout.CENTER);
		
		Box box = Box.createHorizontalBox();
		box.add(Box.createVerticalStrut(IConstants.V_GAP));
		outerPane.add(box, BorderLayout.NORTH);

		box = Box.createHorizontalBox();
		box.add(Box.createVerticalStrut(IConstants.V_GAP));
		outerPane.add(box, BorderLayout.SOUTH);
		
		box = Box.createVerticalBox();
		box.add(Box.createHorizontalStrut(IConstants.H_GAP));
		outerPane.add(box, BorderLayout.WEST);

		box = Box.createVerticalBox();
		box.add(Box.createHorizontalStrut(IConstants.H_GAP));
		outerPane.add(box, BorderLayout.EAST);
		
		return outerPane;
	}
	
	
}
