package auroras.OpenJDR.client.ui;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import auroras.OpenJDR.client.defaultImpl.controller.undoRedo.ActionQueue;
import auroras.OpenJDR.client.interfaces.controller.IGameController;
import auroras.OpenJDR.client.interfaces.view.ITile;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.common.utils.IconsAndImages;
import auroras.OpenJDR.common.utils.IconsAndImages.TileBackground;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;

public class CheatBox extends Box implements ITranslatableElement {
	
	private JButton undoButton;
	private JButton undoButtonGray;
	private JButton redoButton;
	private JButton redoButtonGray;
	
	
	
	public CheatBox() {
		super(BoxLayout.X_AXIS);
	}
	
	/** TODO: add action listeners */
	@Override
	public void buildUI(Language lang) {
		GameLogger.getLogger(LogModule.VIEW).debug("adding cheat box");
		this.removeAll();
		undoButton = new JButton(IconsAndImages.undoIcon);
		undoButton.setToolTipText(Lang.get("Undo action", lang));
		this.add(undoButton);
		
		undoButtonGray = new JButton(IconsAndImages.undoIconGray);
		undoButtonGray.setToolTipText(Lang.get("Cannot undo action", lang));
		this.add(undoButtonGray);
		
		redoButton = new JButton(IconsAndImages.redoIcon);
		redoButton.setToolTipText(Lang.get("Redo action", lang));
		this.add(redoButton);
		
		redoButtonGray = new JButton(IconsAndImages.redoIconGray);
		redoButtonGray.setToolTipText(Lang.get("Cannot redo action", lang));
		this.add(redoButtonGray);
		
		IconsAndImages.removeButtonDecorators(undoButton, undoButtonGray, redoButton, redoButtonGray);
	}

	public void updateButtonsVisibility(ActionQueue actionQueue) {
		undoButton.setVisible(actionQueue.canUndo());
		undoButtonGray.setVisible(!actionQueue.canUndo());
		redoButton.setVisible(actionQueue.canRedo());
		redoButtonGray.setVisible(!actionQueue.canRedo());
	}
	
	
}
