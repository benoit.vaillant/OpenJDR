package auroras.OpenJDR.client.ui.icons;

import java.awt.Dimension;

import javax.swing.ImageIcon;

import auroras.OpenJDR.common.utils.IconsAndImages;
import auroras.OpenJDR.common.utils.IconsAndImages.Rotation;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class PlayIcon extends DefaultIcon {

	private static ImageIcon icon;
	private Dimension size;
	
	public PlayIcon(Rotation rotation) {
		super(rotation);
		if(icon==null)
			icon=IconsAndImages.createImageIcon("/resources/png/ui/icons/play.png");
	}
	
	@Override
	public ImageIcon getIcon() {
		return icon;
	}
	
	@Override
	public Dimension getSize() {
		if(size==null)
			return SMALL_ICON_DIMENSION;
		return this.size;
	}
	
}
