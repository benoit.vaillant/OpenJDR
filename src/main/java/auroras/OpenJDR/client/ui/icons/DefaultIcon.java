package auroras.OpenJDR.client.ui.icons;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import auroras.OpenJDR.common.utils.IconsAndImages;
import auroras.OpenJDR.common.utils.IconsAndImages.Rotation;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class DefaultIcon extends JComponent {

	public static int ICON_SIZE=32;
	public static Dimension DEFAULT_ICON_DIMENSION=new Dimension(ICON_SIZE, ICON_SIZE);
	public static Dimension SMALL_ICON_DIMENSION=new Dimension(ICON_SIZE/2, ICON_SIZE/2);
	
	
	private static ImageIcon icon;
	
	private Rotation rotation;
	
	private Dimension dimension;

	/** empty icon */
	public DefaultIcon() {
		if(icon==null)
			icon = IconsAndImages.createImageIcon("/resources/png/ui/icons/noIcon.png");
		rotation=Rotation.NONE;
	}

	/** default icon constructor */
	public DefaultIcon(Rotation rotation) {
		if(icon==null)
			icon = IconsAndImages.createImageIcon("/resources/png/ui/icons/noIcon.png");
		this.rotation=rotation;
	}

	/** should be used for non repeating icons */
	public DefaultIcon(String file, Rotation rotation) {
		icon = IconsAndImages.createImageIcon(file);
		this.rotation=rotation;
	}
	

	public void updateRotation(Rotation rotation){
		this.rotation=rotation;
	}
	
	/** to override to get the right icon */
	public ImageIcon getIcon() {
		return icon;
	}

	public Rotation getRotation() {
		return rotation;
	}
	
	@Override
	public void paint(Graphics g) {
		//super.paint(g);
		paintIcon((Graphics2D)g);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		//super.paintComponent(g);
		paintIcon((Graphics2D)g);
	}
	
	public Dimension getSize() {
		if(dimension==null) {
			return DEFAULT_ICON_DIMENSION;
		}
		return dimension;
	}
	
	protected void paintIcon(Graphics2D g2d) {
		//rotate
		g2d.rotate(Rotation.convertDegreesToRadians(rotation.getRotationAngle()),icon.getIconHeight()/2, icon.getIconWidth()/2);
		//scale if needed
		Image img = getIcon().getImage();
		if(!this.getSize().equals(new Dimension(ICON_SIZE, ICON_SIZE))) {
			g2d.drawImage(img,0, (int)getSize().getHeight()/2, (int)getSize().getWidth(), (int)getSize().getHeight(),null);
		}else {
			g2d.drawImage(img, 0, 0, null);
		}
	}
	
}
