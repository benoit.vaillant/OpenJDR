package auroras.OpenJDR.client.ui.events;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import auroras.OpenJDR.client.ui.IConstants;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class EventsAndChatPane extends JPanel implements ITranslatableElement {

	private ArrayList<String> channelBuffer;
	private static final int bufferSize=128;
	private JTextField inputField;
	private JTextArea chat;

	public EventsAndChatPane() {
		super(new BorderLayout());
		channelBuffer=new ArrayList<String>();
	}

	public void buildUI(Language lang) {
		this.removeAll();
		chat = new JTextArea();
		chat.setRows(4);
		chat.setEditable(false);
		for(String s:channelBuffer)
			chat.append(s+"\n");
		JScrollPane pane = new JScrollPane(chat);
		this.add(pane, BorderLayout.CENTER);

		Box bottomBox=Box.createHorizontalBox();
		bottomBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
		bottomBox.add(new JLabel(Lang.get("Chat", lang)));
		bottomBox.add(Box.createHorizontalStrut(IConstants.H_GAP));

		inputField = new JTextField();
		inputField.addKeyListener(chatEntryEnvent());
		bottomBox.add(inputField);
		//bottomBox.add(Box.createHorizontalStrut(IConstants.H_GAP));

		this.add(bottomBox, BorderLayout.SOUTH);
	}

	private KeyListener chatEntryEnvent() {
		return new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					addChatEntry(inputField.getText()+"\n");
					inputField.setText("");
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

		};
	}

	/** TODO: get the document and remove to many lines */
	private void addChatEntry(String s) {
		if(channelBuffer.size()>bufferSize) {
			channelBuffer.remove(bufferSize);
		}
		channelBuffer.add(s);
		chat.append(s);
	}

}
