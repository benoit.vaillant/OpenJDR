package auroras.OpenJDR.client.ui.character;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;

import auroras.OpenJDR.client.abstractImpl.model.AbstractCharacterImpl;
import auroras.OpenJDR.client.defaultImpl.view.CharacterView;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.ui.IConstants;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;

public class CharacterFullView extends JPanel implements ITranslatableElement {

	private ICharacter character;
	private CharacterView view;
	
	public CharacterFullView(ICharacter character, CharacterView view) {
		super(new BorderLayout());
		this.setBackground(null);
		this.character=character;
		this.view=view;
		buildUI(Language.EN);
	}
	
	@Override
	public void buildUI(Language lang) {
		this.removeAll();
		this.add(new JLabel(view.getPersoImage()), BorderLayout.CENTER);
		if(character instanceof AbstractCharacterImpl) {
			AbstractCharacterImpl aci = (AbstractCharacterImpl) character;
			if(aci.getPvMax()>0 && aci.getManaMax()>0) {
				GameLogger.getLogger(LogModule.VIEW).debug("Adding life and mana");
				this.add(this.createCaractBox(), BorderLayout.WEST);
				this.add(this.createCaractBox(), BorderLayout.EAST);
			}
		}
	}

	private Box createCaractBox() {
		Box vBox= Box.createVerticalBox();
		int height = (int)this.getPreferredSize().getHeight();
		Box emptyBox = Box.createHorizontalBox();
		emptyBox.setPreferredSize(new Dimension(IConstants.H_GAP, height));
		vBox.add(emptyBox);
		return vBox;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(character instanceof AbstractCharacterImpl) {
			AbstractCharacterImpl aci = (AbstractCharacterImpl) character;
			if(aci.getPvMax()>0 && aci.getManaMax()>0) {
				int height = this.getPreferredSize().height;
				g.setColor(Color.red);
				g.drawRect(0, 0, IConstants.H_GAP, height);
				g.fillRect(0, 0, IConstants.H_GAP, aci.getPv()/aci.getPvMax()*height);
				g.setColor(Color.blue);
				g.drawRect((int)this.getPreferredSize().getWidth()-1, 0, IConstants.H_GAP, this.getPreferredSize().height);
				g.fillRect((int)this.getPreferredSize().getWidth()-1, 0, IConstants.H_GAP, aci.getMana()/aci.getManaMax()*height);
			}
		}
	}	
}
