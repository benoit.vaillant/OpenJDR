package auroras.OpenJDR.client.ui.tiles;

import javax.swing.ImageIcon;

import auroras.OpenJDR.common.utils.IconsAndImages;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public enum TileEditorLocation {

	MAIN_MAP(IconsAndImages.createImageIcon("/resources/png/tiles/DefaultTile.png"), "Main Map"),
	SOFT_DUNGEON(IconsAndImages.createImageIcon("/resources/png/ui/icons/noIcon.png"), "Soft Dugeon"),
	DARK_DUNGEON(IconsAndImages.createImageIcon("/resources/png/ui/icons/noIcon.png"), "Dark Dungeon"),
	;
	
	private String uiTranslatableName;
	private ImageIcon uiIcon;
	
	private TileEditorLocation(ImageIcon uiIcon, String uiTranslatableName) {
		this.uiIcon=uiIcon;		
		this.uiTranslatableName=uiTranslatableName;
	}

	public String getUiTranslatableName() {
		return uiTranslatableName;
	}

	public ImageIcon getUiIcon() {
		return uiIcon;
	}
	
}
