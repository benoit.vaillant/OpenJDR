package auroras.OpenJDR.client.ui.tiles;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JComponent;

import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.model.IItem;
import auroras.OpenJDR.client.interfaces.view.ITile;
import auroras.OpenJDR.common.utils.IconsAndImages;
import auroras.OpenJDR.common.utils.IconsAndImages.Rotation;
import auroras.OpenJDR.common.utils.IconsAndImages.TileBackground;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public abstract class AbstractTile extends JComponent implements ITile {

	public static int TILE_SIZE=64;
	
	private TileBackground tileBackground;
	protected Rotation rotation;
	
	protected ArrayList<ICharacter> characters;
	protected ArrayList<IItem> items;
	
	public AbstractTile(TileBackground tile, Rotation rotation) {
		this.tileBackground=tile;
		this.rotation=rotation;
		this.characters=new ArrayList<>();
		this.items=new ArrayList<>();
	}

	@Override
	public Dimension getPreferredSize() {		
		return new Dimension(TILE_SIZE,TILE_SIZE);
	}
	
	@Override
	public Dimension getMinimumSize() {		
		return new Dimension(TILE_SIZE,TILE_SIZE);
	}

	@Override
	public Dimension getMaximumSize() {
		return new Dimension(TILE_SIZE,TILE_SIZE);
	}
	
	@Override
	public void addCharacter(ICharacter character) {
		characters.add(character);		
	}
	
	@Override
	public boolean removeCharacter(ICharacter character) {		
		return characters.remove(character);
	}
	
	@Override
	public int getCharacterNumber() {
		return characters.size();
	}

	@Override
	public boolean hasGroup() {		
		return characters.size()>1;
	}
	
	@Override
	public boolean hasItems() {		
		return items.size()>0;
	}	
	
	@Override
	public void addItem(IItem item) {
		items.add(item);
	}
	
	@Override
	public boolean removeItem(IItem item) {		
		return items.remove(item);
	}

	@Override
	public TileBackground getTileBackground() {
		return tileBackground;
	}
	
	protected void paintOriented(Graphics g) {
		//TODO
		g.drawImage(tileBackground.getImageIcon().getImage(), 0, 0, null);
	}
	
	@Override
	public void paint(Graphics g) {
		paintOriented(g);
		if(characters.size()>0) {
			if(hasGroup()) {
				g.drawImage(IconsAndImages.groupImageIcon.getImage(), 0, 0, null);
			} else {
				g.drawImage(IconsAndImages.playerImageIcon.getImage(), 0, 0, null);				
			}
		}
	}
	
}
