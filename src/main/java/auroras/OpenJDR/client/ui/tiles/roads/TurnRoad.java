package auroras.OpenJDR.client.ui.tiles.roads;

import auroras.OpenJDR.client.ui.tiles.AbstractTile;
import auroras.OpenJDR.common.utils.IconsAndImages;
import auroras.OpenJDR.common.utils.IconsAndImages.Rotation;
import auroras.OpenJDR.common.utils.IconsAndImages.TileBackground;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class TurnRoad extends AbstractTile {

	public TurnRoad(Rotation rotation) {
		super(IconsAndImages.TileBackground.TURN_ROAD, rotation);
	}

	@Override
	public boolean canRotate() {
		return true;
	}
	
}
