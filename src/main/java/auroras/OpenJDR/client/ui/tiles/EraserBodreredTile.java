package auroras.OpenJDR.client.ui.tiles;

import java.awt.Graphics;
import java.awt.Image;

import auroras.OpenJDR.common.utils.IconsAndImages;
import auroras.OpenJDR.common.utils.IconsAndImages.Rotation;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class EraserBodreredTile extends AbstractTile {

	public static Image eraserImage;
	
	public EraserBodreredTile(Rotation rotation) {
		super(IconsAndImages.TileBackground.EMPTY_BORDERED_TILE, Rotation.NONE);	
		if(eraserImage==null)
			eraserImage = IconsAndImages.createImageIcon("/resources/png/ui/icons/openclipart/Eraser-2.png").getImage();
	}
	
	@Override
	public boolean canRotate() {
		return false;
	}

	@Override
	public void paint(Graphics g) {
		g.drawImage(eraserImage, 0, 0, null);
		super.paint(g);
	}
}
