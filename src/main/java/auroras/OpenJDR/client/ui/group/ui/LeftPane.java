package auroras.OpenJDR.client.ui.group.ui;

import java.awt.BorderLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import auroras.OpenJDR.client.interfaces.controller.IGameController;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.client.ui.MainView;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.common.utils.IconsAndImages;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class LeftPane extends JPanel implements ITranslatableElement {

	private Language lang;

	private JTabbedPane tabs;

	private GroupPane charactersPane;
	private DetailedCharacterPane detailedCharacterPane;

	public LeftPane() {
		super(new BorderLayout());

		tabs = new JTabbedPane();

		charactersPane = new GroupPane();
		detailedCharacterPane=new DetailedCharacterPane();

		this.add(tabs, BorderLayout.CENTER);
	}

	public void buildUI(Language lang) {
		this.lang=lang;

		tabs.removeAll();
		tabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);

		charactersPane.buildUI(lang);
		detailedCharacterPane.buildUI(lang);

		tabs.addTab(null, IconsAndImages.groupImageIcon, charactersPane);
		tabs.addTab(null, IconsAndImages.playerImageIcon, detailedCharacterPane);


	}

	public void updateSelectedCharacter(ICharacter character) {
		detailedCharacterPane.setCharacter(character);
		detailedCharacterPane.buildUI(lang);
	}

}
