package auroras.OpenJDR.client.ui.group.creation;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.EventObject;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.CellEditorListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.jdesktop.swingx.JXTable;

import auroras.OpenJDR.client.abstractImpl.view.AbstractCharacterView;
import auroras.OpenJDR.client.abstractImpl.view.CharacterImplView;
import auroras.OpenJDR.client.defaultImpl.model.CharacterGroup;
import auroras.OpenJDR.client.defaultImpl.view.CharacterView;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.view.ICharacterView;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.client.ui.icons.DefaultIcon;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.game.OpenJDRGame;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;
import javafx.util.converter.CharacterStringConverter;
import net.miginfocom.swing.MigLayout;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class GroupComposition extends JPanel implements ITranslatableElement {

	private Language lang;

	private CharacterGroup characterGroup;
	
	private JScrollPane pane;
	//private JPanel innerPanel;
	
	private JXTable innerTable;
	private DefaultTableModel tableModel;
	
	public GroupComposition() {
		super(new BorderLayout());
		
		//innerPanel = new JPanel();
		innerTable = new JXTable() {
			@Override
			public Class<?> getColumnClass(int column) {
				if(column==0)
					return AbstractCharacterView.class;
				return JPanel.class;
			}
			
			@Override
			public boolean isCellEditable(int row, int column) {
				if(column==0)
					return true;
				return false;
			}
			
		};

		innerTable.setDefaultRenderer(JPanel.class, new TableCellRenderer() {
			
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
					int row, int column) {
				JPanel pane = (JPanel)value;
				return pane;
			}
		});
		
		innerTable.setDefaultRenderer(AbstractCharacterView.class, new TableCellRenderer() {
			
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
					int row, int column) {
				JPanel pane = (JPanel)value;
				return pane;
			}
		});
		
		innerTable.setDefaultEditor(AbstractCharacterView.class, new CharacterCellEditor());
		
		innerTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		innerTable.setShowGrid(true);

		
		pane = new JScrollPane(innerTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

		characterGroup=new CharacterGroup();
		
		this.add(pane, BorderLayout.CENTER);
	}

	@Override
	public void buildUI(Language lang) {
		this.lang=lang;
		rebuildUI(lang);
	}
	
	private void rebuildUI(Language lang) {
		//innerPanel.removeAll();
		
		Object[][] data = new Object[characterGroup.size()][3];
		
		for(int i=0; i<characterGroup.size();i++) {
			ICharacter c = characterGroup.getCharater(i);
			ICharacterView cView = characterGroup.getView(c);
			cView.setEditableName(true);
			cView.buildUI(lang);
			//cView.getNameEditButton().addActionListener(editName());
			data[i][0]=cView.getCaracterRepr();
			data[i][1]=cView.getCaracteristicsRepr();
			data[i][2]=cView.getCompetencesRepr();
		}
		tableModel=new DefaultTableModel(data, new String[] {"","",""});
		innerTable.setModel(tableModel);
		innerTable.setTableHeader(null);
		/** TODO: set the row height to correctly fit */
		if(innerTable.getRowCount()>0) {
			JPanel cView = (JPanel) innerTable.getValueAt(0, 0);
			
			GameLogger.getLogger(LogModule.VIEW).debug("Setting height: %d", new Double(cView.getPreferredSize().getHeight()).intValue());
			innerTable.setRowHeight(new Double(cView.getPreferredSize().getHeight()).intValue());
		}
		innerTable.revalidate();
	}

	private ActionListener editName() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(innerTable.getSelectedRow());
			}
		};
	}

	public void addCharacter(ICharacter c) {
		characterGroup.add(c);
		rebuildUI(lang);
	}
	
}
