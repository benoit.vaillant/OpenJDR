package auroras.OpenJDR.client.ui.group.creation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;

import auroras.OpenJDR.client.factories.characters.DefaultCharacters;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.client.ui.group.ui.GroupPane;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class AvailableCharacters extends JPanel implements ITranslatableElement {

	private GroupPane groupPane;
	private GroupComposition composition;
	
	public AvailableCharacters() {
		super(new BorderLayout());
	}

	@Override
	public void buildUI(Language lang) {
		this.removeAll();		
		groupPane = new GroupPane();
		groupPane.buildUI(lang);
		//FIXME : adapt size
		groupPane.setPreferredSize(new Dimension(148, 148));
		groupPane.setMinimumSize(new Dimension(148, 148));
		this.add(groupPane, BorderLayout.WEST);
		composition = new GroupComposition();
		composition.buildUI(lang);
		this.add(composition, BorderLayout.CENTER);		
	}
	

	public ListSelectionModel getListSelectionModel() {
		return groupPane.getSelectionModel();
	}
	
	public int getSelectedRow() {
		return groupPane.getSelectedRow();
	}

	public void add(ICharacter c) {
		composition.addCharacter(c);
	}
	
}
