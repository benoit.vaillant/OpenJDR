package auroras.OpenJDR.client.ui.group.creation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import auroras.OpenJDR.client.interfaces.model.Caracteristic;
import auroras.OpenJDR.client.interfaces.model.ICaracteristics;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.common.utils.Dices;
import auroras.OpenJDR.game.OpenJDRGame;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class GroupCreation extends JFrame implements ITranslatableElement {

	private Language lang;
	private JPanel mainPane;

	private JMenuItem addMenu;
	private JMenuItem removeMenu;
	private JMenuItem saveMenu;
	
	private AvailableCharacters innerPane;
	
	public GroupCreation(Language lang) {
		super();
		GameLogger.getLogger(LogModule.VIEW).debug("Starting group creation");
		this.lang=lang;
		setPreferredSize(new Dimension(640, 480));
		setTitle(Lang.get("Group creation", lang));
		mainPane=new JPanel(new BorderLayout());
		this.setContentPane(mainPane);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);		
	}
	
	@Override
	public void buildUI(Language lang) {
		mainPane.removeAll();		
		mainPane.add(charmenu(), BorderLayout.NORTH);
		innerPane =charView(); 
		mainPane.add(innerPane, BorderLayout.CENTER);
		
	}


	private JMenuBar charmenu() {
		JMenuBar menubar = new JMenuBar();
		addMenu = new JMenuItem(Lang.get("Add", lang));
		addMenu.setEnabled(false);
		removeMenu = new JMenuItem(Lang.get("Remove", lang));
		removeMenu.setEnabled(false);
		saveMenu = new JMenuItem(Lang.get("Save", lang));
		
		addMenu.addActionListener(createRandomizedCharacter());
		
		menubar.add(addMenu);
		menubar.add(removeMenu);
		menubar.add(saveMenu);
		menubar.add(Box.createHorizontalGlue());
		return menubar;
	}

	private AvailableCharacters charView() {
		innerPane = new AvailableCharacters();
		innerPane.buildUI(lang);
		innerPane.getListSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(!e.getValueIsAdjusting()) {
					addMenu.setEnabled(innerPane.getSelectedRow()>=0);
				}
			}
		});
		
		return innerPane;
	}

	private ActionListener createRandomizedCharacter() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ICharacter c = OpenJDRGame.getController().getMainModel().getCharacetrModelViewMapping().getCharacter(innerPane.getSelectedRow());
				c=c.duplicate();
				GroupCreation.randomize(c);
				innerPane.add(c);
			}
		};
	}

	protected static void randomize(ICharacter c) {		
		for(Caracteristic caract : c.getCaracts().getCaracts().keySet()) {
			int val =Dices.rollDice(2);
			int ret;
			if(val==1) { //minus
				ret=-Dices.rollDice(2);
			}else { //plus
				ret=Dices.rollDice(2);				
			}
			int cVal = c.getCaracts().getCaracts().get(caract)+ret;
			c.getCaracts().getCaracts().put(caract, cVal);
		}
		
	}
	
}
