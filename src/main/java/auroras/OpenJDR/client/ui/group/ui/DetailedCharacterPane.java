package auroras.OpenJDR.client.ui.group.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.lang.reflect.InvocationTargetException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import auroras.OpenJDR.client.abstractImpl.view.CharacterImplView;
import auroras.OpenJDR.client.defaultImpl.Character;
import auroras.OpenJDR.client.defaultImpl.view.CharacterView;
import auroras.OpenJDR.client.interfaces.controller.IGameController;
import auroras.OpenJDR.client.interfaces.model.ICaracteristics;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.model.ICompetences;
import auroras.OpenJDR.client.interfaces.model.IInventory;
import auroras.OpenJDR.client.interfaces.view.ICharacterView;
import auroras.OpenJDR.client.interfaces.view.ICompetencesView;
import auroras.OpenJDR.client.interfaces.view.IInventoryView;
import auroras.OpenJDR.client.interfaces.view.IcaracteristicsView;
import auroras.OpenJDR.client.ui.IConstants;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.client.ui.MainView;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.game.OpenJDRGame;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class DetailedCharacterPane extends JPanel implements ITranslatableElement {

	private ICharacter character;

	DetailedCharacterPane() {
		super(new BorderLayout());
	}

	public void buildUI(Language lang) {
		GameLogger.getLogger(LogModule.VIEW).debug("Refreshing character pane");
		this.removeAll();
		JPanel innerPanel=new JPanel(new BorderLayout());

		if(character!=null) {
			Box vBox = Box.createVerticalBox();

			try {
				Box hBox = Box.createHorizontalBox();
				hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
				hBox.add(new JLabel(character.getName()));
				hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
				hBox.add(Box.createHorizontalGlue());
				hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
				hBox.add(new JLabel(Lang.get("Lvl", lang)+character.getLevel()));
				hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
				vBox.add(hBox);

				IcaracteristicsView carsView = (IcaracteristicsView) OpenJDRGame.getController().getImpl("caracteristicsView").getDeclaredConstructor(ICaracteristics.class).newInstance(character.getCaracts());
				carsView.buildUI(lang);
				vBox.add(carsView.getView());

				ICompetencesView cView = (ICompetencesView) OpenJDRGame.getController().getImpl("competencesView").getDeclaredConstructor(ICompetences.class).newInstance(character.getCompetences());
				cView.buildUI(lang);
				vBox.add(cView.getView());

				IInventoryView iView = (IInventoryView) OpenJDRGame.getController().getImpl("inventoryView").getDeclaredConstructor(IInventory.class).newInstance(character.getInventory());
				iView.buildUI(lang);
				vBox.add(iView.getView());

				vBox.add(Box.createVerticalGlue());
			} catch (InstantiationException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				GameLogger.getLogger(LogModule.VIEW).error("Could not create character pane: %s", e.getMessage());
			}

			innerPanel.add(vBox, BorderLayout.CENTER);
		} else {
			Box vBox = Box.createVerticalBox();
			vBox.add(new JLabel(Lang.get("No character selected", lang)));
			vBox.add(Box.createVerticalGlue());
			innerPanel.add(vBox, BorderLayout.CENTER);
		}

		JScrollPane scrolly=new JScrollPane(innerPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.add(scrolly, BorderLayout.CENTER);
	}

	public void setCharacter(ICharacter character) {
		this.character = character;
	}

}
