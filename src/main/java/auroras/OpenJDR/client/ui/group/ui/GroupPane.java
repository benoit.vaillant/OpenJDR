package auroras.OpenJDR.client.ui.group.ui;

import java.awt.BorderLayout;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.jdesktop.swingx.JXTable;

import auroras.OpenJDR.client.abstractImpl.view.CharacterImplView;
import auroras.OpenJDR.client.defaultImpl.model.CharacterGroup;
import auroras.OpenJDR.client.defaultImpl.view.CharacterView;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.client.ui.utils.JCharacterCellRenderer;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.game.OpenJDRGame;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class GroupPane extends JPanel implements ITranslatableElement {

	private JXTable charsTable;

	public GroupPane() {
		super(new BorderLayout());
	}

	public void buildUI(Language lang) {
		this.removeAll();
		
		CharacterGroup charactersGroup = OpenJDRGame.getController().getMainModel().getCharacetrModelViewMapping().getCurrentCharacters();

		ICharacter[][] characters=new ICharacter[charactersGroup.size()][1];

		for(int i=0;i<charactersGroup.size();i++) {
			characters[i][0]=charactersGroup.get(i);
		}


		DefaultTableModel model= new DefaultTableModel(characters, new String[]{""});
		charsTable = new JXTable(model){
			@Override
			public Class<?> getColumnClass(int column) {
				return ICharacter.class;
			}
		};

		charsTable.setDefaultRenderer(ICharacter.class, new JCharacterCellRenderer());
		charsTable.setTableHeader(null);
		charsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		charsTable.setShowGrid(true);

		ListSelectionModel cellSelectionModel = charsTable.getSelectionModel();
		cellSelectionModel.addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent e) {
				if(charsTable.getSelectedRow()>=0) {
					OpenJDRGame.getController().updateSelectedCharacter((ICharacter)charsTable.getValueAt(charsTable.getSelectedRow(), 0));
				}else {
					OpenJDRGame.getController().updateSelectedCharacter(null);
				}
			}
		});

		/** set the row height to correctly fit */
		if(charsTable.getRowCount()>1) {
			ICharacter c = (ICharacter) charsTable.getValueAt(0, 0);
			try {
				CharacterImplView cView = (CharacterView) OpenJDRGame.getController().getImpl("characterView").getDeclaredConstructor(ICharacter.class).newInstance(c);
				charsTable.setRowHeight(cView.getHeight());
			} catch (InstantiationException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e1) {
				GameLogger.getLogger(LogModule.VIEW).error("Could not set the row height: %s", e1.getMessage());
			}
		}


		JScrollPane charsPane = new JScrollPane(charsTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		this.add(charsPane, BorderLayout.CENTER);
	}

	public ListSelectionModel getSelectionModel() {
		return charsTable.getSelectionModel();
	}
	
	public int getSelectedRow() {
		return charsTable.getSelectedRow();
	}
	
}
