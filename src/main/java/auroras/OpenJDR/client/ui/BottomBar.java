package auroras.OpenJDR.client.ui;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;

import auroras.OpenJDR.client.interfaces.controller.IGameController;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class BottomBar extends Box implements ITranslatableElement {


	public BottomBar() {
		super(BoxLayout.X_AXIS);
		this.setBorder(BorderFactory.createLoweredBevelBorder());

	}

	public void buildUI(Language lang) {
		this.removeAll();
		this.add(Box.createHorizontalStrut(IConstants.H_GAP));
		JLabel time = new JLabel(Lang.get("Time:", lang));
		this.add(time);
		this.add(Box.createHorizontalStrut(IConstants.H_GAP));
		this.add(Box.createHorizontalGlue());
		this.add(Box.createHorizontalStrut(IConstants.H_GAP));
	}

}
