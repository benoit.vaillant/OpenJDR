package auroras.OpenJDR.client.ui;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import auroras.OpenJDR.client.interfaces.controller.IGameController;
import auroras.OpenJDR.client.ui.generic.AbstractActionPanel;
import auroras.OpenJDR.client.ui.generic.AbstractMissionPanel;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class RightPane extends JPanel implements ITranslatableElement {
	
	private DefaultActionPanel actionPanel;
	private DefaultMissionPanel missionPanel;
	
	public RightPane() {
		super();
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		actionPanel=new DefaultActionPanel();
		missionPanel=new DefaultMissionPanel();
	}
	
	private class DefaultMissionPanel extends AbstractMissionPanel {
		public DefaultMissionPanel() {
			super();
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public void checkEnd() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private class DefaultActionPanel extends AbstractActionPanel {

		public DefaultActionPanel() {
			super();
			// TODO Auto-generated constructor stub
		}
		
	}
	
	
	
	@Override
	public void buildUI(Language lang) {
		removeAll();
		actionPanel.buildUI(lang);
		missionPanel.buildUI(lang);
		this.add(actionPanel);
		this.add(missionPanel);
		
	}

	
	
	
	
}
