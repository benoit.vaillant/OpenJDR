package auroras.OpenJDR.client.ui;

import java.awt.BorderLayout;
import java.util.concurrent.ExecutionException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.ui.group.ui.LeftPane;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.common.utils.FileUtils;
import auroras.OpenJDR.common.utils.IconsAndImages;
import auroras.OpenJDR.game.OpenJDRGame;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class MainView extends JFrame implements ITranslatableElement {


	private Language lang;

	private TopMenu topMenu;
	private BottomBar bottomBar;
	private LeftPane leftPane;
	private RightPane rightPane;
	private CenterPane centerPane;

	public MainView(Language lang) {
		IconsAndImages.init(this);
		FileUtils.init(this);
	}
	
	public void buildUI(Language lang) {
		this.lang=lang;
		
		SwingWorker<JPanel, Void> worker=new SwingWorker<JPanel, Void>() {
			@Override
			protected JPanel doInBackground() throws Exception {
				JPanel mainPane = new JPanel(new BorderLayout());

				topMenu = new TopMenu();
				bottomBar = new BottomBar();
				leftPane=new LeftPane();
				rightPane=new RightPane();
				centerPane=new CenterPane();

				topMenu.buildUI(lang);
				bottomBar.buildUI(lang);
				leftPane.buildUI(lang);
				rightPane.buildUI(lang);
				centerPane.buildUI(lang);
				centerPane.setMap(OpenJDRGame.getController().getMap());

				mainPane.add(topMenu, BorderLayout.NORTH);
				mainPane.add(leftPane, BorderLayout.WEST);
				mainPane.add(rightPane, BorderLayout.EAST);
				mainPane.add(bottomBar, BorderLayout.SOUTH);				
				mainPane.add(centerPane, BorderLayout.CENTER);
				
				return mainPane;
			}
			
			protected void done() {
				try {
					MainView.this.setContentPane(get());
					MainView.this.setVisible(true);
				} catch (InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			};
		};
		
		worker.execute();
		
	}

	public void updateSelectedCharacter(ICharacter character) {
		leftPane.updateSelectedCharacter(character);
	}
	
	public Language getLang() {
		return lang;
	}

	public void setLang(Language lang) {
		this.lang = lang;
	}

	public CheatBox getCheatBox() {
		return topMenu.getCheatBox();
	}

}
