package auroras.OpenJDR.client.defaultImpl.model.characters;

import javax.swing.Icon;

import auroras.OpenJDR.client.abstractImpl.model.AbstractCharacterImpl;
import auroras.OpenJDR.client.defaultImpl.Caracts;
import auroras.OpenJDR.client.defaultImpl.Competences;
import auroras.OpenJDR.client.defaultImpl.Inventory;
import auroras.OpenJDR.client.defaultImpl.model.characters.Human.HumanCaracts;
import auroras.OpenJDR.client.defaultImpl.model.characters.Human.HumanCompetences;
import auroras.OpenJDR.client.interfaces.model.Caracteristic;
import auroras.OpenJDR.client.interfaces.model.ICaracteristics;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.model.ICompetences;
import auroras.OpenJDR.client.interfaces.model.IInventory;
import auroras.OpenJDR.common.utils.IconsAndImages;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class Troll extends AbstractCharacterImpl {

	public static class TrollCaracts extends Caracts {
		public TrollCaracts() {
			this.put(Caracteristic.intelligence, 3);
			this.put(Caracteristic.strength, 8);
			this.put(Caracteristic.view, 4);
			this.put(Caracteristic.agility, 8);
			this.put(Caracteristic.mouvementAllowance, 8);
			this.put(Caracteristic.charisme, 4);
		}
	}

	public static class TrollCompetences extends Competences {
		//TODO		
	}

	public Troll() {
		super("Troll", new TrollCaracts(), new TrollCompetences(), new Inventory());
	}

	public Troll(String name, ICaracteristics caracts, ICompetences competences, IInventory inventory) {
		super(name, caracts, competences, inventory);
		setPvMax(45);
		setManaMax(5);
		setPv(45);
		setMana(5);
	}

	@Override
	public String getIconImagePath() {
		if(characterIcon==null)
			return "/resources/png/character/icons/troll.png";
		return characterIcon;
	}
	
	@Override
	public ICharacter duplicate() {
		Troll t = new Troll(new String(name), caracts.duplicate(), competences.duplicate(), inventory.duplicate());
		t.copyOtherInfos(this);
		return t;
	}
	
}
