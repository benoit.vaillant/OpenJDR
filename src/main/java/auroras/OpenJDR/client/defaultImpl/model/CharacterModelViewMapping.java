package auroras.OpenJDR.client.defaultImpl.model;

import java.util.ArrayList;

import auroras.OpenJDR.client.defaultImpl.Caracts;
import auroras.OpenJDR.client.defaultImpl.Competences;
import auroras.OpenJDR.client.defaultImpl.Inventory;
import auroras.OpenJDR.client.defaultImpl.model.characters.Barbarian;
import auroras.OpenJDR.client.defaultImpl.model.characters.Elfe;
import auroras.OpenJDR.client.defaultImpl.model.characters.Gnome;
import auroras.OpenJDR.client.defaultImpl.model.characters.Human;
import auroras.OpenJDR.client.defaultImpl.model.characters.Troll;
import auroras.OpenJDR.client.defaultImpl.model.characters.Wizard;
import auroras.OpenJDR.client.defaultImpl.view.characters.BarbarianView;
import auroras.OpenJDR.client.defaultImpl.view.characters.ElfeView;
import auroras.OpenJDR.client.defaultImpl.view.characters.GnomeView;
import auroras.OpenJDR.client.defaultImpl.view.characters.HumanView;
import auroras.OpenJDR.client.defaultImpl.view.characters.TrollView;
import auroras.OpenJDR.client.defaultImpl.view.characters.WizardView;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.view.ICharacterView;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class CharacterModelViewMapping {

	private static CharacterGroup defaultCharacters;
	
	private CharacterGroup currentCharacters;
	
	public CharacterModelViewMapping() {
		currentCharacters=new CharacterGroup();
		defaultCharacters= new CharacterGroup();
		defaultCharacters.add(new Barbarian());
		defaultCharacters.add(new Elfe());
		defaultCharacters.add(new Gnome());
		defaultCharacters.add(new Human());
		defaultCharacters.add(new Troll());
		defaultCharacters.add(new Wizard());
	}
		
	public ICharacter getDefaultCharacter(ICharacterView view) {
		return defaultCharacters.getDefaultCharacter(view);
	}
	
	public ICharacterView getDefaultView(ICharacter model) {
		return defaultCharacters.getDefaultView(model);
	}

	public ICharacter getCharacter(ICharacterView view) {
		return currentCharacters.getCharacter(view);
	}
	
	public ICharacterView getView(ICharacter character) {
		return currentCharacters.getView(character);		
	}
	

	public ICharacter getCharacter(int idx) {
		return defaultCharacters.get(idx);
	}

	public CharacterGroup getCurrentCharacters() {
		return currentCharacters;
	}

	public void setCurrentCharacters(CharacterGroup currentCharacters) {
		this.currentCharacters = currentCharacters;
	}

	public static CharacterGroup getDefaultCharacters() {
		return defaultCharacters;
	}
	
}
