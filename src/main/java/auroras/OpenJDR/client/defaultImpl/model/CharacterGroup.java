package auroras.OpenJDR.client.defaultImpl.model;

import java.util.ArrayList;

import auroras.OpenJDR.client.defaultImpl.view.CharacterView;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.view.ICharacterView;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class CharacterGroup {
	
	private ArrayList<ICharacter> characterModels;
	private ArrayList<CharacterView> characterViews;

	private boolean dirty;
	
	public CharacterGroup() {
		characterModels=new ArrayList<>();
		characterViews=new ArrayList<>();		
	}
	
	public void add(ICharacter c) {
		characterModels.add(c);
		characterViews.add(new CharacterView(c));
	}
	
	public int size() {
		return characterModels.size();
	}
	
	public ICharacter getCharater(int idx) {
		return characterModels.get(idx);
	}

	public ICharacter getDefaultCharacter(ICharacterView view) {
		int ret =-1;
		for (int i = 0; i < characterViews.size(); i++) {
			if(characterViews.get(i).getClass().equals(view.getClass())) {
				ret=i;
				break;
			}
		}
		if(ret<0)
			return null;
		return characterModels.get(ret);
	}
	
	public ICharacterView getDefaultView(ICharacter model) {
		int ret =-1;
		for (int i = 0; i < characterModels.size(); i++) {
			if(characterModels.get(i).getClass().equals(model.getClass())) {
				ret=i;
				break;
			}
		}
		if(ret<0)
			return null;
		return characterViews.get(ret);
	}

	public ICharacter getCharacter(ICharacterView view) {
		for (int i = 0; i < characterViews.size(); i++) {
			if(characterViews.get(i)==view) // we do not want equals here
				return characterModels.get(i);
		}
		return null;
	}
	
	public ICharacterView getView(ICharacter character) {
		for (int i = 0; i < characterModels.size(); i++) {
			if(characterModels.get(i)==character) // we do not want equals here
				return characterViews.get(i);
		}
		return null;		
	}
	
	
	public boolean isDirty() {
		return dirty;
	}

	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	public ICharacter get(int idx) {
		return characterModels.get(idx);
	}
	
}
