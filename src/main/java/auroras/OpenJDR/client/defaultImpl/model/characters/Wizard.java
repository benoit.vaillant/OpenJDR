package auroras.OpenJDR.client.defaultImpl.model.characters;

import javax.swing.Icon;

import auroras.OpenJDR.client.abstractImpl.model.AbstractCharacterImpl;
import auroras.OpenJDR.client.defaultImpl.Caracts;
import auroras.OpenJDR.client.defaultImpl.Competences;
import auroras.OpenJDR.client.defaultImpl.Inventory;
import auroras.OpenJDR.client.defaultImpl.model.characters.Troll.TrollCaracts;
import auroras.OpenJDR.client.defaultImpl.model.characters.Troll.TrollCompetences;
import auroras.OpenJDR.client.interfaces.model.Caracteristic;
import auroras.OpenJDR.client.interfaces.model.ICaracteristics;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.model.ICompetences;
import auroras.OpenJDR.client.interfaces.model.IInventory;
import auroras.OpenJDR.common.utils.IconsAndImages;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class Wizard extends AbstractCharacterImpl {

	public static class WizardCaracts extends Caracts {
		public WizardCaracts() {
			this.put(Caracteristic.intelligence, 10);
			this.put(Caracteristic.strength, 3);
			this.put(Caracteristic.view, 6);
			this.put(Caracteristic.agility, 5);
			this.put(Caracteristic.mouvementAllowance, 4);
			this.put(Caracteristic.charisme, 7);
		}
	}

	public static class WizardCompetences extends Competences {
		//TODO		
	}

	public Wizard() {
		super("Wizard", new WizardCaracts(), new WizardCompetences(), new Inventory());
	}

	
	
	public Wizard(String name, ICaracteristics caracts, ICompetences competences, IInventory inventory) {
		super(name, caracts, competences, inventory);
		setPvMax(10);
		setManaMax(40);
		setPv(10);
		setMana(40);
	}

	@Override
	public String getIconImagePath() {
		if(characterIcon==null)
			return "/resources/png/character/icons/wizard.png";
		return characterIcon;
	}
	
	@Override
	public ICharacter duplicate() {
		Wizard w = new Wizard(new String(name), caracts.duplicate(), competences.duplicate(), inventory.duplicate());
		w.copyOtherInfos(this);
		return w;
	}
	
}
