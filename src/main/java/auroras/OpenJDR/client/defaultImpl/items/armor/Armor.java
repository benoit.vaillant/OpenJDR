package auroras.OpenJDR.client.defaultImpl.items.armor;

import auroras.OpenJDR.client.defaultImpl.Character.BodyLocation;
import auroras.OpenJDR.client.defaultImpl.items.AbstractItem;
import auroras.OpenJDR.client.interfaces.model.IItem;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class Armor extends AbstractItem {

	@Override
	protected BodyLocation[] getDestinationLocations() {
		return new BodyLocation[]{BodyLocation.TORSO};
	}

	@Override
	public IItem duplicate() {
		Armor armor = new Armor();
		return armor;
	}
	
}
