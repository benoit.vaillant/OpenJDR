package auroras.OpenJDR.client.defaultImpl.items;

import auroras.OpenJDR.client.defaultImpl.Item;
import auroras.OpenJDR.client.defaultImpl.Location;
import auroras.OpenJDR.client.defaultImpl.Character.BodyLocation;
import auroras.OpenJDR.client.exceptions.equipment.EquipFailureException;
import auroras.OpenJDR.client.interfaces.model.IInventory;
import auroras.OpenJDR.client.interfaces.model.IItem;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public abstract class AbstractItem extends Item {

	protected Language lang;
	
	public void buildUI(Language lang) {
		this.lang=lang;
	}
	
	protected abstract BodyLocation[] getDestinationLocations();
	
	public void equip(IInventory inventory) throws EquipFailureException {
		BodyLocation[] dest=getDestinationLocations();
		
		for(IItem item:inventory.equipedItems()) {
			BodyLocation[] worn= ((Location)((Item)item).getLocation()).getWorn();
			for(BodyLocation currentLocOccupied:worn) {
				for(BodyLocation destDesired:dest) {
					if(dest.equals(currentLocOccupied))
						throw new EquipFailureException(lang, "Cannot equip there");
				}
			}
		}
	}
	
}
