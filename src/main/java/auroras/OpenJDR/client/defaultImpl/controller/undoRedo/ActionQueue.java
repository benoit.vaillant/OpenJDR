package auroras.OpenJDR.client.defaultImpl.controller.undoRedo;

import java.util.ArrayList;

import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;

import auroras.OpenJDR.client.interfaces.controller.IAction;
import auroras.OpenJDR.client.interfaces.controller.IGameController;

public class ActionQueue extends AbstractUndoableEdit implements UndoableEditListener {

	private static UndoRedoManager undoRedoManager;
	
	public ActionQueue() {
		undoRedoManager = new UndoRedoManager();
	}
	
	@Override
	public synchronized boolean canUndo() {
		return undoRedoManager.canUndo();
	}
	
	@Override
	public synchronized boolean canRedo() {
		return undoRedoManager.canRedo();
	}

	@Override
	public void undoableEditHappened(UndoableEditEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
