package auroras.OpenJDR.client.defaultImpl.controller;

import java.util.ArrayList;
import java.util.HashMap;

import auroras.OpenJDR.client.abstractImpl.view.CharacterImplView;
import auroras.OpenJDR.client.defaultImpl.Caracts;
import auroras.OpenJDR.client.defaultImpl.Competences;
import auroras.OpenJDR.client.defaultImpl.Inventory;
import auroras.OpenJDR.client.defaultImpl.controller.undoRedo.ActionQueue;
import auroras.OpenJDR.client.defaultImpl.model.MainModel;
import auroras.OpenJDR.client.defaultImpl.view.CaracteristicsView;
import auroras.OpenJDR.client.defaultImpl.view.CharacterView;
import auroras.OpenJDR.client.defaultImpl.view.CompetencesView;
import auroras.OpenJDR.client.defaultImpl.view.InventoryView;
import auroras.OpenJDR.client.interfaces.controller.IGameController;
import auroras.OpenJDR.client.interfaces.map.IFullMap;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.model.IQuest;
import auroras.OpenJDR.client.ui.MainView;
import auroras.OpenJDR.game.OpenJDRGame;
import auroras.OpenJDR.client.defaultImpl.Character;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class DefaultGameController implements IGameController {

	private static MainView mainView;
	private static MainModel mainModel;

	private HashMap<String, Class> gameImpl;
	
	private ArrayList<IQuest> quests;
	
	private ActionQueue actionQueue;
	
	public DefaultGameController(MainView mainView, MainModel mainModel) {
		DefaultGameController.mainView=mainView;
		DefaultGameController.mainModel=mainModel;
		quests=new ArrayList<>();
		actionQueue=new ActionQueue();		
	}
	
	@Override
	public void loadGameImpl() {
		loadDefaultGameImpl();		
	}
	
	public void loadDefaultGameImpl() {
		gameImpl=new HashMap<String, Class>();
		// model
		gameImpl.put("character", Character.class);
		gameImpl.put("competences", Competences.class);
		gameImpl.put("inventory", Inventory.class);
		gameImpl.put("caracteristics", Caracts.class);
		//view
		gameImpl.put("characterView", CharacterView.class);
		gameImpl.put("competencesView", CompetencesView.class);
		gameImpl.put("inventoryView", InventoryView.class);
		gameImpl.put("caracteristicsView", CaracteristicsView.class);
	}

	public Class getImpl(String c) {
		return gameImpl.get(c);
	}

	@Override
	public void setMap(IFullMap map) {
		mainModel.setMap(map);
	}

	@Override
	public IFullMap getMap() {
		return mainModel.getMap();
	}
	
	@Override
	public void updateSelectedCharacter(ICharacter character) {
		mainView.updateSelectedCharacter(character);		
	}
	
	public MainView getMainView() {
		return mainView;
	}
	
	@Override
	public MainModel getMainModel() {
		return mainModel;
	}
	
	@Override
	public void quit() {
		System.exit(0);		
	}
	
	@Override
	public ArrayList<IQuest> getQuests() {
		return quests;
	}

	@Override
	public boolean hasQuests() {
		return quests!=null && quests.size()>0;
	}

	@Override
	public void setCheatMode(boolean on) {
		mainView.getCheatBox().setVisible(on);
	}
	
	@Override
	public void updateUndoRedo() {
		mainView.getCheatBox().updateButtonsVisibility(actionQueue);
	}

	@Override
	public void undo() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void redo(boolean reroll) {
		// TODO Auto-generated method stub
		
	}
	
}