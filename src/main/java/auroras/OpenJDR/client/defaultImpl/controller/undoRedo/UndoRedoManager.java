package auroras.OpenJDR.client.defaultImpl.controller.undoRedo;

import java.util.ArrayList;
import java.util.List;

import auroras.OpenJDR.client.interfaces.controller.IAction;
import auroras.OpenJDR.client.interfaces.controller.IGameController;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class UndoRedoManager {
	
	private List<IAction> replayManager;	
	private int offset;

	public UndoRedoManager() {
		replayManager=new ArrayList<>();
		offset=0;
	}
		
	public boolean canUndo() {
		if(offset==0)
			return false;
		return replayManager.get(offset).canUndo();
	}
	
	public boolean canRedo() {
		if(offset==replayManager.size())
			return false;
		return replayManager.get(offset).canRedo();
	}
	
	/** discard undone actions */
	public void addAction(IAction action) {
		replayManager= replayManager.subList(0, offset);
		replayManager.add(action);
		offset++;
	}
	
	public int getTotalLength() {
		return replayManager.size();
	}
	
	public int getLength() {
		return offset;
	}
	

}
