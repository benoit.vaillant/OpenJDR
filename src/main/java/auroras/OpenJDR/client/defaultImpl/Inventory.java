package auroras.OpenJDR.client.defaultImpl;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

import auroras.OpenJDR.client.interfaces.model.IInventory;
import auroras.OpenJDR.client.interfaces.model.IItem;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class Inventory implements IInventory {

	private Language lang;

	private ArrayList<IItem> equipedItems;
	private ArrayList<IItem> stashedItems;
	private int gold;

	public Inventory() {
		equipedItems=new ArrayList<>();
		stashedItems=new ArrayList<>();
		gold=0;
	}

	public void buildUI(Language lang) {
		this.lang=lang;
	}

	public IItem[] equipedItems() {
		return equipedItems.toArray(new IItem[]{});
	}

	public IItem[] stashedItems() {
		return stashedItems.toArray(new IItem[]{});
	}

	public int getGold() {
		return gold;
	}

	public JPanel getBodyRepr() {
		JPanel pane=new JPanel(new BorderLayout());
		pane.add(new JLabel("Body Repr TODO"));
		return pane;
	}

	public JPanel getInventoryRepr() {
		JPanel pane=new JPanel(new BorderLayout());
		pane.add(new JLabel("Inventory Repr TODO"));
		pane.add(new JLabel("gold: "+getGold()));
		return pane;
	}
	
	@Override
	public IInventory duplicate() {
		Inventory inv = new Inventory();
		inv.equipedItems=new ArrayList<>();
		for(IItem item:equipedItems)
			inv.equipedItems.add(item.duplicate());
		inv.stashedItems=new ArrayList<>();
		for(IItem item:stashedItems)
			inv.stashedItems.add(item.duplicate());
		inv.gold=gold;
		return inv;
	}

}
