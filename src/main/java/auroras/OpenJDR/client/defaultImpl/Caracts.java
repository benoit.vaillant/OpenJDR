package auroras.OpenJDR.client.defaultImpl;

import java.util.HashMap;

import auroras.OpenJDR.client.interfaces.model.Caracteristic;
import auroras.OpenJDR.client.interfaces.model.ICaracteristics;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class Caracts implements ICaracteristics {

	private HashMap<Caracteristic, Integer> caractsMap;

	public Caracts() {
		caractsMap = getEmptyCaracts();
	}
	
	public static HashMap<Caracteristic, Integer> getEmptyCaracts() {
		HashMap<Caracteristic, Integer> caractsMap = new HashMap<>();
		for(Caracteristic caracteristic:Caracteristic.values()) {
			caractsMap.put(caracteristic,0);
		}

		return caractsMap;
	}

	public int put(Caracteristic key, int value) {
		return caractsMap.put(key, value);
	}

	@Override
	public HashMap<Caracteristic, Integer> getCaracts() {
		return caractsMap;
	}

	@Override
	public ICaracteristics duplicate() {
		Caracts c= new Caracts();
		c.caractsMap=(HashMap<Caracteristic, Integer>) caractsMap.clone();
		return c;
	}
	
}
