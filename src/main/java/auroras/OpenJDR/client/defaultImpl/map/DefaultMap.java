package auroras.OpenJDR.client.defaultImpl.map;

import java.util.ArrayList;

import auroras.OpenJDR.client.interfaces.map.IFullMap;
import auroras.OpenJDR.client.interfaces.map.IMap;
import auroras.OpenJDR.client.interfaces.model.IDungeon;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class DefaultMap implements IFullMap {

	private IMap generalMap;
	private ArrayList<IDungeon> dungeons;

	private boolean generalMapShowing;

	public DefaultMap() {
		generalMap=new DefaultGeneralMap();
		generalMapShowing=true;
		dungeons=new ArrayList<>();
	}

	@Override
	public boolean hasGeneralMap() {
		return true;
	}

	@Override
	public IMap getGeneralMap() {
		return (DefaultGeneralMap)generalMap;
	}


	public boolean isGeneralMapShowing() {
		return generalMapShowing;
	}

	public void setGeneralMapShowing(boolean generalMapShowing) {
		this.generalMapShowing = generalMapShowing;
	}
	
	@Override
	public boolean hasFogOfWar() {
		return false;
	}

	@Override
	public ArrayList<IDungeon> getDungeons() {
		return dungeons;
	}

	@Override
	public void setGeneralMap(IMap generalMap) {
		this.generalMap=generalMap;
		
	}

	@Override
	public void setDungeons(ArrayList<IDungeon> dungeons) {
		this.dungeons=dungeons;		
	}
	
}
