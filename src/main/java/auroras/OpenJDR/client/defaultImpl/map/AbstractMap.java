package auroras.OpenJDR.client.defaultImpl.map;

import java.util.ArrayList;
import java.util.HashMap;

import com.sun.javafx.iio.ImageStorage.ImageType;

import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.view.ITile;
import auroras.OpenJDR.client.ui.tiles.DefaultTile;
import auroras.OpenJDR.common.utils.IconsAndImages;
import auroras.OpenJDR.common.utils.IconsAndImages.Rotation;
import auroras.OpenJDR.common.utils.IconsAndImages.TileBackground;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public abstract class AbstractMap {

	protected ITile[][] tiles;
	protected Rotation[][] rotations;
	
	protected HashMap<String, ArrayList<ICharacter>> pj;
	protected ArrayList<ICharacter> pnj;
	
	/** an empty map */
	public AbstractMap(int width, int height) {
		tiles=new ITile[width][height];
		rotations=new Rotation[width][height];
		for (int i = 0; i < tiles.length; i++) {
			for (int j = 0; j < tiles[0].length; j++) {
				tiles[i][j]=new DefaultTile(Rotation.NONE);
				rotations[i][j]=Rotation.NONE;
			}
		}
	}
	
	public AbstractMap(ITile[][] tiles, Rotation[][] rotations) {
		this.tiles=tiles;
		this.rotations=rotations;
		
	}
	
	public ITile getTileAt(int x, int y) {
		return tiles[x][y];
	}

	public Rotation getTileRotationAt(int x, int y) {
		return rotations[x][y];
	}

	public int getWidth() {
		return tiles.length;
	}
	
	public int getHeight() {
		return tiles[0].length;
	}
	

	
}
