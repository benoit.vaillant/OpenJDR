package auroras.OpenJDR.client.defaultImpl.map;
import auroras.OpenJDR.client.interfaces.map.IMap;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.view.ITile;
import auroras.OpenJDR.common.utils.IconsAndImages.Rotation;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class DefaultGeneralMap extends AbstractMap implements IMap {

	public DefaultGeneralMap() {
		super(80, 60);
	}

	public DefaultGeneralMap(ITile[][] tiles, Rotation[][] rotations) {
		super(tiles, rotations);		
	}
	
	@Override
	public void setCharacterAt(int x, int y, ICharacter character) {
		//TODO
		
	}
	
}
