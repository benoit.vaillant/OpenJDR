package auroras.OpenJDR.client.defaultImpl.map;

import java.awt.Point;
import java.util.ArrayList;

import auroras.OpenJDR.client.interfaces.view.IPath;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class DefaultPath implements IPath {

	ArrayList<Point> path;
	
	public DefaultPath(Point startingPoint) {
		path=new ArrayList<>();
		path.add(startingPoint);
	}
	
	@Override
	public ArrayList<Point> getPath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updatePath(Point tile) {
		// TODO Auto-generated method stub

	}

}
