
package auroras.OpenJDR.client.defaultImpl.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.JXTable;

import auroras.OpenJDR.client.interfaces.model.Caracteristic;
import auroras.OpenJDR.client.interfaces.model.ICaracteristics;
import auroras.OpenJDR.client.interfaces.view.IcaracteristicsView;
import auroras.OpenJDR.client.ui.IConstants;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class CaracteristicsView extends JPanel implements IcaracteristicsView {

	private ICaracteristics caracteristics;

	public CaracteristicsView(ICaracteristics caracteristics) {
		super(new BorderLayout());
		this.setBorder(BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
		this.caracteristics=caracteristics;
	}

	public void buildUI(Language lang) {
		this.removeAll();
		this.add(new JLabel(Lang.get("Caracteristics",lang)), BorderLayout.NORTH);

		Object[][] data = new Object[Caracteristic.values() .length][2];
		for (int i = 0; i < Caracteristic.values().length; i++) {
			data[i][0]=Lang.get(Caracteristic.values()[i].getName(), lang);
			data[i][1]=caracteristics.getCaracts().get(Caracteristic.values()[i]);
		}

		DefaultTableModel dataModel = new DefaultTableModel(data, new String[]{"", ""});
		JXTable caracteristicsTable = new JXTable(dataModel);

		caracteristicsTable.getColumnExt(1).setPreferredWidth(IConstants.NUMBER_WIDTH);
		
		this.add(caracteristicsTable, BorderLayout.CENTER);
	}

	public JPanel getView() {
		//TODO: set better sizes
		this.setPreferredSize(new Dimension(180,160));
		return this;
	}

	
}
