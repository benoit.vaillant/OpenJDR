package auroras.OpenJDR.client.defaultImpl.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.beans.Transient;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.JXTable;

import com.sun.java.swing.plaf.motif.MotifBorders.BevelBorder;

import auroras.OpenJDR.client.interfaces.model.ICompetences;
import auroras.OpenJDR.client.interfaces.view.ICompetencesView;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class CompetencesView extends JPanel implements ICompetencesView {

	private ICompetences competences;

	public CompetencesView(ICompetences competences) {
		super(new BorderLayout());
		this.setBorder(BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
		this.competences=competences;
	}

	public void buildUI(Language lang) {
		this.removeAll();

		this.add(new JLabel(Lang.get("Competences", lang)), BorderLayout.NORTH);

		ArrayList<Object[]> data = new ArrayList<Object[]>();
		for (int i = 0; i < competences.getCompetenceKeyNamesInOrder().length; i++) {
			if(competences.getCompetences().get(competences.getCompetenceKeyNamesInOrder()[i])!=null) {
				Object[] line = new Object[2];
				line[0]=Lang.get(competences.getCompetenceKeyNamesInOrder()[i], lang);
				line[1]=competences.getCompetences().get(competences.getCompetenceKeyNamesInOrder()[i]);
				data.add(line);
			}
		}

		DefaultTableModel dataModel = new DefaultTableModel(data.toArray(new Object[][]{}), new String[]{"", ""});
		JXTable caracteristicsTable = new JXTable(dataModel);

		this.add(caracteristicsTable, BorderLayout.CENTER);
		this.revalidate();

	}


	public CompetencesView getView() {
		return this;
	}

}
