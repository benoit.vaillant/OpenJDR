package auroras.OpenJDR.client.defaultImpl.view;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;

import auroras.OpenJDR.client.interfaces.model.IInventory;
import auroras.OpenJDR.client.interfaces.view.IInventoryView;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.common.utils.IconsAndImages;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class InventoryView extends JPanel implements IInventoryView {

	private IInventory inventory;

	public InventoryView(IInventory inventory) {
		super(new BorderLayout());
		this.setBorder(BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
		this.inventory=inventory;
	}

	public void buildUI(Language lang) {
		this.removeAll();

		Box vBox = Box.createVerticalBox();
		//equiped
		JPanel equipedPanel = new JPanel(new BorderLayout());
		equipedPanel.add(new JLabel(Lang.get("Equiped", lang)), BorderLayout.NORTH);


		vBox.add(equipedPanel);
		//stashed
		JPanel stashedPanel = new JPanel(new BorderLayout());
		equipedPanel.add(new JLabel(Lang.get("Stash", lang)),BorderLayout.NORTH);

		vBox.add(stashedPanel);
		//pack
		this.add(vBox, BorderLayout.CENTER);
		this.revalidate();
		this.repaint();
	}

	public InventoryView getView() {
		return this;
	}


}
