package auroras.OpenJDR.client.defaultImpl.view.characters;

import java.awt.BorderLayout;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import auroras.OpenJDR.client.defaultImpl.view.CharacterView;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.ui.IConstants;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.client.ui.character.CharacterFullView;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;

public class CharacterRepresentation extends JPanel implements ITranslatableElement {

	private ICharacter character;
	private CharacterView view;
	
	private JTextField tf;
	
	private boolean editable;
	
	public CharacterRepresentation(ICharacter character, CharacterView view) {
		super(new BorderLayout());
		this.character=character;
		this.view=view;
		editable=false;
	}
	
	@Override
	public void buildUI(Language lang) {
		GameLogger.getLogger(LogModule.VIEW).debug("Building character repr");
		this.removeAll();
		//caractPane = new JPanel(new BorderLayout());
		Box hBox = Box.createHorizontalBox();
		hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
		if(editable) {
			tf = new JTextField(character.getName());
			hBox.add(tf);
		} else {
			hBox.add(new JLabel(character.getName()));
		}
		hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
		hBox.add(Box.createHorizontalGlue());	
		hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
		hBox.add(new JLabel(Lang.get("Lvl", lang)+character.getLevel()));
		hBox.add(Box.createHorizontalStrut(IConstants.H_GAP));
		this.add(hBox, BorderLayout.NORTH);
		CharacterFullView cView = new CharacterFullView(character, view);
		this.add(cView, BorderLayout.CENTER);		
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public void setNewName(String newName) {
		character.setName(tf.getText());
	}

	public String getEditedName() {
		return tf.getText();
	}
	
}
