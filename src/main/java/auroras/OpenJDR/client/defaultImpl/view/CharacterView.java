package auroras.OpenJDR.client.defaultImpl.view;

import javax.swing.Icon;
import javax.swing.JPanel;

import auroras.OpenJDR.client.abstractImpl.view.CharacterImplView;
import auroras.OpenJDR.client.defaultImpl.view.characters.CharacterRepresentation;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.ui.ITranslatableElement;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.common.utils.IconsAndImages;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class CharacterView extends CharacterImplView implements ITranslatableElement {

	private CharacterRepresentation caracterRepr;
	private CaracteristicsView caracteristicsView;
	private CompetencesView competencesView;
	private InventoryView inventoryView;
	protected Icon characterIcon;
	
	public CharacterView(ICharacter character) {		
		super(character);
		GameLogger.getLogger(LogModule.VIEW).warn("Building character view for character: %s", character.getName());
		this.character = character;
		caracterRepr=new CharacterRepresentation(character,this);
		//TODO: use the default file configuration
		caracterRepr.buildUI(Language.EN);
		caracteristicsView = new CaracteristicsView(character.getCaracts());
		competencesView = new CompetencesView(character.getCompetences());
		inventoryView = new InventoryView(character.getInventory());
	}


	public void buildUI(Language lang) {
		caracterRepr.buildUI(lang);
		caracteristicsView.buildUI(lang);
		competencesView.buildUI(lang);
		inventoryView.buildUI(lang);
	}

	public JPanel getCaracterRepr() {
		return caracterRepr;
	}

	public JPanel getCaracteristicsRepr() {
		return caracteristicsView.getView();
	}

	public JPanel getCompetencesRepr() {
		return competencesView.getView();
	}

	/** returns an array for the character representation and the stashed items */
	public JPanel getInventoryRepr() {
		return inventoryView.getView();
	}

	public JPanel getView() {
		return caracterRepr;
	}


	@Override
	public Icon getPersoImage() {
		if(characterIcon==null)
			characterIcon=IconsAndImages.createImageIcon(character.getIconImagePath());
		return characterIcon;
	}

	@Override
	public void setEditableName(boolean edit) {
		caracterRepr.setEditable(edit);		
	}
	
}
