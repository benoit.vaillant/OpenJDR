package auroras.OpenJDR.client.defaultImpl;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JComponent;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.JXTable;

import auroras.OpenJDR.client.interfaces.model.ICompetences;
import auroras.OpenJDR.common.i18n.Lang;
import auroras.OpenJDR.common.i18n.Lang.Language;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class Competences implements ICompetences {

	private HashMap<String, Integer> competences;

	private static String[] competencesKeys = {
			"fight sword",
			"defend",
	};

	public Competences() {
		competences=new HashMap<String, Integer>();
	}


	public HashMap<String, Integer> getCompetences() {
		return competences;
	}

	public String[] getCompetenceKeyNamesInOrder() {
		return competencesKeys;
	}

	@Override
	public ICompetences duplicate() {
		Competences comps=new Competences();
		comps.competences=(HashMap<String, Integer>) competences.clone();
		return comps;
	}


}
