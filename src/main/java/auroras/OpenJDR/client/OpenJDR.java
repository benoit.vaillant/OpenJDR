package auroras.OpenJDR.client;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import auroras.OpenJDR.client.defaultImpl.controller.DefaultGameController;
import auroras.OpenJDR.client.defaultImpl.map.DefaultMap;
import auroras.OpenJDR.client.defaultImpl.model.MainModel;
import auroras.OpenJDR.client.factories.characters.DefaultCharacters;
import auroras.OpenJDR.client.factories.map.RandomGeneralMapGenerator;
import auroras.OpenJDR.client.interfaces.controller.IGameController;
import auroras.OpenJDR.client.ui.MainView;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.game.OpenJDRGame;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class OpenJDR
{
	
	private static int MAJOR=0;
	private static int MINOR=0;
	private static int REVISION=1;
	
	private static Language lang;
	private static MainView mainView;
	private static MainModel mainModel;
	private static OpenJDRGame openJRDGame;

	public OpenJDR(Language lang) {
		this.lang=lang;
		
		//make a splashscreen
		SwingWorker<OpenJDRGame, Void> loader = initSplashLoader();		
		
		Thread initThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				loader.execute();						
			}
		});
		initThread.run();
		
		try {
			openJRDGame=loader.get();
		} catch (InterruptedException | ExecutionException e) {
			GameLogger.getLogger(LogModule.CONTROLLER).error("Could not load controller, exiting.");
			System.exit(-1);
		}
		
		//TODO: remove
		openJRDGame.initDefaultGroup();
		DefaultCharacters.init();

		MainView mainView = OpenJDRGame.getController().getMainView();
		((DefaultGameController)OpenJDRGame.getController()).loadDefaultGameImpl();
		mainView.setPreferredSize(new Dimension(800, 600));
		mainView.setMinimumSize(new Dimension(640, 480));
		mainView.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainView.setTitle("OpenJDR");		
		mainView.buildUI(lang);
       
		//TODO: remove
		OpenJDRGame.getController().setMap(new DefaultMap());
	}

	
	public static Constructor[] getConstructorsInOrder() {
		try {
			return new Constructor[] {
					OpenJDRGame.class.getDeclaredConstructor(),
					MainView.class.getDeclaredConstructor(Language.class),
					MainModel.class.getDeclaredConstructor(),
					DefaultGameController.class.getDeclaredConstructor(MainView.class, MainModel.class),
			};
		} catch (NoSuchMethodException | SecurityException e) {
			GameLogger.getLogger(LogModule.MAIN).error("Constructor failure, exiting: %s", e.getMessage());
			System.exit(-2);
		}
		//never reached
		return null;
	}


	private SwingWorker<OpenJDRGame, Void> initSplashLoader() {
		URL url = this.getClass().getResource("/resources/various/splash.png");
		auroras.OpenJDR.SplashScreen splashScreen = new auroras.OpenJDR.SplashScreen(lang, url);

		SwingWorker<OpenJDRGame, Void> worker = new SwingWorker<OpenJDRGame, Void>() {
			@Override
			protected OpenJDRGame doInBackground() throws Exception {
				
				try {
					splashScreen.setVisible(true);

					Constructor[] constructors = getConstructorsInOrder();
					splashScreen.update("loading the game");
					openJRDGame=(OpenJDRGame) constructors[0].newInstance();
					splashScreen.update("loading the main view");
					mainView=(MainView) constructors[1].newInstance(lang);
					splashScreen.update("loading the main model");
					mainModel=(MainModel) constructors[2].newInstance();
					splashScreen.update("loading the controller");
					IGameController controller = (IGameController) constructors[3].newInstance(mainView, mainModel);
					OpenJDRGame.setController(controller);
					return openJRDGame;
				} catch ( SecurityException e) {
					GameLogger.getLogger(LogModule.MAIN).error("Game instanciation error, exiting: %s", e.getMessage());
					System.exit(-3);
				}
				//never reached
				return null;
			}
			
			@Override
			protected void done() {
				super.done();	
				splashScreen.dispose();
			}
		};
		
		return worker;
		
	}
	
	public static void main( String[] args )
	{
		try {
			GameLogger.init();
		} catch (Exception e) {
			System.out.println("GameLogger class not initialized, aborting.");
			e.printStackTrace();
			System.exit(-1);
		}
		GameLogger.getLogger(LogModule.MAIN).info("Logging module started");
		
		String lookAndFeel = "Nimbus";
		Language lang = Language.EN;
		
		createOrLoadMainProperties();
		
		RandomGeneralMapGenerator.Ground.regenerateGroundHelper();
		
		try {
			GameLogger.getLogger(LogModule.MAIN).info("Loading look & feel");
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if (lookAndFeel.equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Throwable t) {
			GameLogger.getLogger(LogModule.MAIN).error("Look and feel not loaded: "+t.getMessage());
		}

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				OpenJDR ojdr = new OpenJDR(lang);
			}
		});
	}

	/** this file should contain the less information as possible */
	private static void createOrLoadMainProperties() {
		GameLogger.getLogger(LogModule.MAIN).info("Loading ini file");
		String home = System.getProperty("user.home");
		File iniFile=new File(home, ".OpenJDR");
		if(!iniFile.exists()) {
			try {
				if(!iniFile.createNewFile()) {
					GameLogger.getLogger(LogModule.MAIN).info("Cannot create ini file, aborting.");
					System.exit(-4);
				}
			} catch (IOException e) {
				GameLogger.getLogger(LogModule.MAIN).info("Cannot create ini file, aborting. (%s)", e.getMessage());
				System.exit(-4);
			}
		}
	}


	public static void quit() {
		System.exit(0);
	}

	public static String getGameVersion() {
		return MAJOR+"."+MINOR+"."+REVISION;
	}
	
}