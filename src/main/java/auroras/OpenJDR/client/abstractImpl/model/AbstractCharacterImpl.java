package auroras.OpenJDR.client.abstractImpl.model;

import javax.swing.Icon;

import auroras.OpenJDR.client.defaultImpl.Caracts;
import auroras.OpenJDR.client.defaultImpl.Competences;
import auroras.OpenJDR.client.defaultImpl.Inventory;
import auroras.OpenJDR.client.interfaces.model.ICaracteristics;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.model.ICompetences;
import auroras.OpenJDR.client.interfaces.model.IInventory;
import auroras.OpenJDR.common.utils.IconsAndImages;
import auroras.OpenJDR.logger.GameLogger;
import auroras.OpenJDR.logger.LogModule;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public abstract class AbstractCharacterImpl implements ICharacter {

	protected String name;
	protected int xp;
	protected int pv;
	protected int pvMax;
	protected int mana;
	protected int manaMax;
	protected ICaracteristics caracts;
	protected ICompetences competences;
	protected IInventory inventory;

	protected String characterIcon;

	
	public AbstractCharacterImpl(String name, ICaracteristics caracts, ICompetences competences, IInventory inventory) {
		GameLogger.getLogger(LogModule.MAIN).info("Creating character %s", name);
		this.name=name;
		this.caracts=caracts;
		this.competences=competences;
		this.inventory=inventory;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ICaracteristics getCaracts() {
		return caracts;
	}

	@Override
	public void setName(String name) {
		this.name=name;
	}
	
	@Override
	public ICompetences getCompetences() {
		return competences;
	}

	@Override
	public IInventory getInventory() {
		return inventory;
	}
	
	@Override
	public int getLevel() {
		return xp<10?1:(int)Math.log10(xp);
	}

	public int getXp() {
		return xp;
	}

	public void setXp(int xp) {
		this.xp = xp;
	}

	public int getPv() {
		return pv;
	}

	public void setPv(int pv) {
		this.pv = pv;
	}

	public int getPvMax() {
		return pvMax;
	}

	public void setPvMax(int pvMax) {
		this.pvMax = pvMax;
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}

	public int getManaMax() {
		return manaMax;
	}

	public void setManaMax(int manaMax) {
		this.manaMax = manaMax;
	}
	
	public void copyOtherInfos(AbstractCharacterImpl to) {
		to.setXp(getXp());
		to.setPv(getPv());
		to.setPvMax(getPvMax());
		to.setMana(getMana());
		to.setManaMax(getManaMax());	
	}
	
}
