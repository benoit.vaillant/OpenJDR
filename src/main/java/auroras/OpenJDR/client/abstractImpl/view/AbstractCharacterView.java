package auroras.OpenJDR.client.abstractImpl.view;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.view.ICharacterView;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.common.utils.IconsAndImages;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public abstract class AbstractCharacterView extends Component implements ICharacterView {

	protected Language lang;
	
	private static ImageIcon defaultIcon;

	public AbstractCharacterView() {
	}

	@Override
	public void buildUI(Language lang) {
		this.lang=lang;
	}

	@Override
	public JPanel getView() {
		return new JPanel();
	}

	@Override
	public int getHeight() {
		return 0;
	}

	public static JPanel emptyPane(ICharacter character) {
		JPanel emptyPane = new JPanel(new BorderLayout());
		emptyPane.add(new JLabel("undefined"), BorderLayout.NORTH);
		emptyPane.add(new JLabel(getDefaultPersoImage()), BorderLayout.CENTER);
		return emptyPane;
	}

	private static ImageIcon getDefaultPersoImage() {
		if(defaultIcon==null)
			defaultIcon=IconsAndImages.createImageIcon("/resources/png/character/icons/icon-person-128.png");
		return defaultIcon;
	}

}
