package auroras.OpenJDR.client.abstractImpl.view;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import auroras.OpenJDR.client.defaultImpl.Competences;
import auroras.OpenJDR.client.defaultImpl.Inventory;
import auroras.OpenJDR.client.interfaces.model.ICaracteristics;
import auroras.OpenJDR.client.interfaces.model.ICharacter;
import auroras.OpenJDR.client.interfaces.view.ICharacterView;
import auroras.OpenJDR.common.i18n.Lang.Language;
import auroras.OpenJDR.common.utils.IconsAndImages;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public abstract class CharacterImplView implements ICharacterView {

	protected Language lang;

	protected ICharacter character;
	
	protected Icon cachedIcon;
	
	public CharacterImplView(ICharacter character) {
		this.character=character;
	}
	
	public int getHeight() {
		int nameHeight = Double.valueOf(new JLabel("fg").getPreferredSize().getHeight()).intValue();
		return getPersoImage().getIconHeight()+nameHeight;
	}
	
	@Override
	public void buildUI(Language lang) {
		this.lang=lang;
	}

	@Override
	public Icon getPersoImage() {
		if(cachedIcon==null) 
			cachedIcon=IconsAndImages.createImageIcon(character.getIconImagePath());
		return cachedIcon;
	}
	
}
