package auroras.OpenJDR.logger;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configurator;

import auroras.OpenJDR.client.OpenJDR;

/**
 * 
 * @author Benoît Vaillant
 *
 * This file is part of OpenJDR.
 *
 * OpenJDR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenJDR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenJDR.  If not, see <http://www.gnu.org/licenses/>.
 */
public class GameLogger {

	private static boolean INITIALIZED = false;
	private static final String CLASSNAME = GameLogger.class.getSimpleName();
	static final String FQCN = GameLogger.class.getName();
	private final Logger logger;
	private final LogModule logModule;
		
	private static final Map<LogModule, GameLogger> GAME_LOGGERS = new HashMap<>(0);

	private GameLogger(LogModule logModule) {
		this.logModule = logModule;
		this.logger = LoggerContext.getContext(false).getLogger(logModule.getName());
	}

	public static GameLogger getLogger(LogModule logModule) {
		if (logModule == null) {
			throw new NullPointerException("logModule cannot be null");
	    }
		GameLogger logger = GAME_LOGGERS.get(logModule);
		if (logger == null) {
			logger = new GameLogger(logModule);
			GAME_LOGGERS.put(logModule, logger);
		}
		return logger;
	}

	
	private final static String LOG4J_CONF_FILE = "/resources/log4j-config.xml";
	
	public synchronized static void init() throws Exception {
		File log4jConfFile = new File(OpenJDR.class.getResource(LOG4J_CONF_FILE).toURI());
		
		LoggerContext context =  (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
		context.setConfigLocation(log4jConfFile.toURI());
		
		if (INITIALIZED) {
			throw new IllegalStateException(CLASSNAME + " has already been initialized.");
		}
		for (LogModule module : LogModule.values()) {
			GameLogger.getLogger(module);
			//log all for the moment by default; initial default is inly error
			GameLogger.getLogger(module).setLevel(Level.ALL);			
		}
		INITIALIZED = true;
	}
	
	public static boolean isInitialized() {
		return INITIALIZED;
	}

	public LogModule getLogModule() {
		return logModule;
	}

	public Level getLevel() {
		return logger.getLevel();
	}

	public void setLevel(Level level) {
		Configurator.setLevel(logModule.getName(), level);
	}

	public void info(String message, Object... args) {
		info(null, message, args);
	}

	public void info(Throwable throwable, String message, Object... parameters) {
		logger.logIfEnabled(FQCN, Level.INFO, null, String.format(message, parameters), throwable);
	}
	
	public void warn(String message, Object... args) {
		warn(null, message, args);
	}

	public void warn(Throwable throwable, String message, Object... parameters) {
		logger.logIfEnabled(FQCN, Level.WARN, null, String.format(message, parameters), throwable);
	}
	
	public void error(String message, Object... args) {
		error(null, message, args);
	}
	
	public void error(Throwable throwable, String message, Object... parameters) {
		logger.logIfEnabled(FQCN, Level.ERROR, null, String.format(message, parameters), throwable);
	}
	
	public void debug(String message, Object... args) {
		debug(null, message, args);
	}

	public void debug(Throwable throwable, String message, Object... parameters) {
		logger.logIfEnabled(FQCN, Level.DEBUG, null, String.format(message, parameters), throwable);
	}

}
